## Class 03: Infinite Loops

**Messy notes:**

**Agenda:**

0. Mini-ex[2] short discussion 
1. Group 1 presentation: Transform...
2. Mini-Lecture + group discussion
3. Sample Code: Decoding exercise
4. Function + Exercise in Class
5. Sample Code: Asterisk Painting + Arrays
6. Loops and While Loop
7. Mini-ex walk-through: Designing a throbber | due SUN mid-night

---

### 0. Mini-ex[2] discussion  

- 1x student will present the miniEx[2] 
- 2x students will talk about the feedback to the class 

---

### 1. Group 1 presentation: Transform...

transform - scale/rotate/translate/push/pop with the text: Femke Snelting, "Other Geometries", transmediale journal, Issue#3, 31 October 2019, https://transmediale.de/content/other-geometries

1. Mikkel Dahlin 
2. Andreas Frederiksen 
3. Clara Lassen 
4. Anne Christiansen

---

### 2. Mini-Lecture + group discussion

- Ada Lovelace - [diagram](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/3-InfiniteLoops/ch3_1.jpg) / loop / cycle 
- Loop and condition
- Throbber as a prominent visual icon - contemporary culture (streams and real-timeness)
    ![](https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/Ap2019/class03/browser1.png)
    ![](https://user-content.gitlab-static.net/e77ec1d81fa507275d6c6fa5fbdc085e86374ffb/68747470733a2f2f6d6f6e647269616e2e6d61736861626c652e636f6d2f75706c6f6164732532353246636172642532353246696d6167652532353246353334373331253235324663303434663930342d373361302d346465342d613034382d3630316536376537393230342e706e672532353246393530783533345f5f66696c7465727325323533417175616c6974792532353238393025323532392e706e673f7369676e61747572653d4d6e693859354666536c373976475651496f433367333939316f673d26736f757263653d6874747073253341253246253246626c75657072696e742d6170692d70726f64756374696f6e2e73332e616d617a6f6e6177732e636f6d)
    
    Buffering icons of death see [here](https://mashable.com/2017/07/12/buffering-icons-ranked/?europe=true#4FSuDdS21Oqr) => many other different names e.g Hourglass cursor, Spinning Wait Cursor, Spinning Beach Ball, Spinning Wheel of Death, etc.
- Opearational and micro(temporal) processes beyond the threshold of human perception
    - streaming
    - example as mentioned in the text: digital signal processing, data packets and network protocols, buffer and buffering
- (Real)Time and Now 

**Group Discussion:** 

Go to: https://padlet.com/siusoon/iaaabkoo9qey

- (group 1-2) class03 : What is the assigned reading about? What is micro-temporal analysis?
- (group 3-4) class03 : What are the things interests you and what are the things you found difficult to grasp? 
- (group 5-6) class03 : What is the role of 'throbber' in this article?
- (group 7-8) class03 : What's the role of coding practice in the article? 
- (group 9-10)class03 : How does digital technology change our perception of time?

---

### 3. Sample Code: Decoding exercise

RUN the RUNME1 in class03- http://siusoon.gitlab.io/aesthetic-programming/: The sample code draws ellipses and performs repeatedly: 

We move from programming static objects such as shapes to a mixture of both static and moving objects. 

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/3-InfiniteLoops/ch3_2.png)

Like previous class, don't look at the source code: 

**Speculation** (13 mins)

Based on what you see/experience on the screen, describe:
- What are the elements? List the items and actions.
    - What is moving and not moving?
    - How many ellipses are there at the center?
    - Try to resize the window and see what happens?

Further questions:
- how do you set the background color?
- how does the ellipse rotate?
- how can you make the ellipse fade out and rotate to the next position?
- how can you position the static yellow lines, as well as the moving ellipses?

**Experimentation** (8 mins)
- Go to this link: https://editor.p5js.org/siusoon/sketches/54qCk_zY
- Try to change some of the parameters e.g. `background()`, `framerate()`, `drawElements()` then press the play button to see the result
- There are some new functions used, can you check these out in the p5.js reference? (e.g. `push()`, `pop()`, `translate()`, `rotate()`)

**Mapping (working with physical paper)** (10 mins)
- Map some of the findings/features from the Speculation that you have done with the source code. Which block of code relates to your findings?
- Can you identify the part/block of code that responds to the elements that you have speculated on earlier?

**Technical questions/issues**
- What's the role of `push()/pop()` here? 
- What does this line mean? (line 18)
`let cir = 360/num*(frameCount%num);`

**Other questions for discussion**
- Where do you often see this icon?
- What do you know about this icon?
- What can't you see?

---

### 4. Function + Exercise in Class

- `function() {}`
    - built-in like `setup()`, `draw()`, `windownResized()`
    - Custom-made like `drawElements()`
- Why? 

**Exercise:**

Type or copy below to your atom's sketch:

```javascript 
let x = sum(4, 3, 2);   
print(x);

function sum(a, b, c) { //passing values 4 as a, 3 as b, 2 as c to the function sum
  return a + b + c; //return statement 
}
```
What's the output of x? 

Exercise 2: Based on the previous example, come up with your new functions and arguments. 

Exercise 3: Back to the [sample code](https://editor.p5js.org/siusoon/sketches/54qCk_zY), instead of having drawElements(), how you might re-arrange the function? 

---

### 5. Sample Code: Asterisk Painting + Arrays

 Artist and software developer John P. Bell has made an artwork called Asterisk Painting, containing a number of throbber-like spinning patterns, but each throbber (or what he calls asterisk) is spinning differently, with variations of color and texture. Many syntaxes that Bell has used are related to time and temporality, for example the setting up of a timer, the calculation of current milliseconds, the speed of rotation, the waiting time for starting a new cycle, and so on. Even for the visual outcome, what constitutes an asterisk is not a geometric shape, but a series of numbers which refer to the milliseconds counter that lines up as a straight line.
 
 ![](https://gitlab.com/siusoon/aesthetic-programming/raw/master/Ap2019/class03/Asterisk_Painting.gif)
 
 **Exercise:** (15 mins)

1. Try to run the Asterisk Painting [here](https://editor.p5js.org/siusoon/sketches/YAk1ZCieC)
2. Try reading the source code above.
3. Using the decoding method that we have introduced earlier, try to speculate, experiment and map your thoughts with the source code.
    - **Speculation**: Describe what see/experience on the screen?
        - What are the elements on the screen?
        - How many asterisks are there on the screen and how are they arranged?
        - What is moving and how do they move?
        - What makes each asterisk spin/rotate and when does it stops to create a new one?
        - Can you locate the time-related syntax in this sketch? 
    - **Experimentation**: Change some of the code parameters
        - Try to change some of the parameters, e.g. the values of the written global variables
        - What are the new syntax and functions that you did not know? (check them out in the p5.js reference?)
    - **Mapping**: Map the elements from speculation to the source code level

**The concept of Arrays:**
- a list of data
- working with a chunk of data 
- related to variables and data types 

```javascript
//example
let words = [] //array -> start with 0
words[0] = "what";
words[1] = "is";
words[2] = "array";
print(words[2]); //output: array
print(words.length); //output: 3
```

Reuse the approach of coding variable: 

1. *Declare:* Think of a name you want to use to store the list of values. The symbol [] in let words = [] indicates words is an array but how many is unknown and hasn't been specified with just this line of code.

2. *Initialize/Assign:* Given the example above, there are three text values to store: "what", "is", and "array". Since array is a list of values and it is needed to be identified individually, an array index is used to represent the position of each piece of data in an array. It starts with [0] as the first item, then [1] as the second, and so forth. Therefore words[0] ="what" means that the first index item of the array words is a string and with the value "what".

3. *Re(use):* The print() function is just an example to indicate how you may retrieve and use the data, you can print it in the console area, or you can arrange to draw on a canvas.
 
To ask how many items in an array, the `syntax arrayname.length` is used.

Example from Aeterisk Painting: 

```javascript
//sample from Asterisk Painting e.g. line 25-26
let xPos = [1,2,3,4,5];
let yPos = [1,2,3,4];
```
*a slightly different way of declaring an array that combines both declare+assign

This also means:

`let xPos = [1,2,3,4,5];` ->
xPos.length: xPos[0] = 1, xPos[1] = 2, xPos[2] = 3, xPos[3] = 4, xPos[4] = 5

`let yPos = [1,2,3,4];` ->
yPos.length: ypos[0] = 1, yPos[1] = 2, yPos[2] = 3, yPos[3] = 4

**index starts with [0]**

Other useful methods: 
- `array.push(value)`  e.g `xPos.push(6)`;
- `array.splice()`

---

### 6. Loops and While Loop

- Why do we need to think about Loops in programming? 

To think about if you need a loop or not, ask yourself: 
- What are the things/actions that you want to loop, to repeat in a sequence or pattern?
- More specifically, what is the conditional logic, and when do you want to exit the loop?
- What do you want to do when this condition is or is not met?

Excerpt from Asterisk Painting:

```javascript
  for(let i=0; i<xPos.length; i++) {
    //calculate the position of each asterisk horizontally as an array that starts with array index[0] 
    xPos[i] = xPos[i] * (xDim / (xPos.length+1));
  }
  
  for(let i=0; i<yPos.length; i++) {  
   //calculate the position of each asterisk vertically as an array that starts with array index[0] 
    yPos[i] = yPos[i] * (yDim / (yPos.length+1));
  }
```

The structure of a for-loop:

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/3-InfiniteLoops/ch3_4.png)

What constitutes a for-loop? 
1. A variable declaration and initialization: Usually starts with 0
2. A specificed condition: The criteria to meet the condition
3. Action: The things that you want to process/compute when the condition is met
4. Loop for next: For the next iteration (usually incremental/decremental)

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/3-InfiniteLoops/ch3_5.png)
*The xPos of each*

**While Loop**

While loop is another type of loop for executing iterations. The statement is executed until the condition is true, it will stop as soon as it is false.

```javascript
while(millis() < wait) {
    
}
```

This example is located towards the end of the program when all the asterisks are drawn but need to wait for a certain time to reset the canvas and restart drawing again. Therefore, this while-loop serves the purpose of a pause sign, freezing the program from running because there is literally nothing between the opening and closing brackets.

---

### 7. Mini-ex walk-through: Designing a Throbber | due SUN mid-night

https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2020/ex

### 7.1 Next class presentation - max 20 mins

Group 2: 
[DOM elements and other form control elements](https://p5js.org/reference/#group-DOM). Using the Button's text from Software Studies as reference, then pick one other form control/DOM element and describe/analyze the specificity of that (such as radio button, slider, checkbox, etc). Go deep to think about the aesthetic issue

*with your own sample code/experimentation - address both the technical understanding and the conceptual thinking with the text*

### Reminder: PCD @ AARHUS 2020  (https://www.pcdaarhus.net/)
