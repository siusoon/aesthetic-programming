**Winnie Soon and Geoff Cox, “[Preface](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/tree/master/source/0-Preface)” in Aesthetic Programming: A Handbook of Software Studies**

*This is an exercise in reflecting and understanding what might be aesthetic programming by close reading the assigned chapter. It involves skills of interpretation, paraphrase, conceptual linkage to your current experience of writing code (RUNME), writing about code (README) and thinking with and through code (other relevant class/instructor materials and examples).*

## Understanding and Interpretation
In groups, read the paragraphs below (and locate it in the source text) and prepare an example that links with your own coding and learning experience/teaching materials/sample code/your own README and RUNME to articulate what that means. 

Group 1 and 10. 
> "computational culture is not just a trendy subject to study, or a way to understand more about what is happening with computational processes, but a means to engage with programming as a way to question existing **technological interfaces** and further **create changes** in the technical system. Thus we consider programming to be a dynamic cultural practice and phenomenon, **a way of thinking and doing in the world**, and a means to understand some of the complex procedures that underwrite our lived realities, in order to act upon those realities."

* What is question existing technological interfaces 
* What it means by create changes
* What it means by "a way of thinking and doing in the world"

Group 2. 
> "aesethetic programming comes close to other phrases such as *creative coding* and *exploratory programming* that have been introduced in related literature in recent years: to emphasize the **expressivity** of computer programming beyond something pragmatic and functional, in which cultural production, or **critical thinking** through the practice of programming, can be cultivated and developed from the broad perspective of the arts and humanities."

* How do you understand Exploratory Programming? (The book from Nick Montfort, and you have read the Appendix chapter "Why Program?")
* What's expressivity means in computer programming?
* What it means by critical thinking through the practice of programming, how might it be cultivated and developed from the perspectives of the arts and humanities? 
* How do you see these in your miniExercises?

Group 3. 
> "It is worth remembering that Adorno and Benjamin famously disagreed on the consequences of this destruction of aura: whilst Benjamin expressed the positive aspects of this shift and the destruction of **aura** is a kind of political emancipation, Adorno expressed the negative view that standardisation and pseudo-individuality would follow. We can clearly see these **tendencies** have accelerated with computational culture and hence the continuing need for sharp critique, and one also based along the lines of "immanent **criticism**" — that which is inherent, as it operates within its object — in the inner workings of software and its material conditions."

* What's an aura here means in the paragraph? (Pls check further the source of materials or search online) 
* What kind of tendency is seeing here? 
* What's criticism?

Group 4.
> "To address this difficulty of upgrade we have been working with fundamental concepts of programming as the starting point for further aesthetic reflection — such as geometry and object abstraction; variables, data types, functions and namings as well as data capturing, processing and automation — thereby the technical functionings are setting the ground works for further understanding of how **cultural phenomena** is constructed and operationalized."

*  What might be cultural phenomena? 

Group 5. 
> "Aesthetic Programming in this sense is considered as a practice to build things but also with the need to produce "reflexive work of critique". This comes close to Philip Agre's notion of "**critical technical practice**" with the drawing together of two different perspectives: formal technical logic and discursive cultural meaning. In other words, this necessitates practical understanding and knowledge of programming to underpin critical understanding of techno-cultural systems, underpinned by levels of expertise in both fields"
    
* Why do you think we need to produce reflexive work of critique?

Group 6.
> "adopting a similar approach of zooming in on the formal logics of computaton and zooming out to the cultural implications of software. In this respect it is also important to recognise that the book *Software Studies* derived from a workshop, and it is worth quoting the project page for its clarity of intention:
    "[T]he project aims at folding the internalist/externalist question of science studies **inside out**, the mechanisms of the one conjugating the subject of the other: what does software-enabled scholarship, in software, art and literary practice have to say about its own **medium**? The purpose of this interaction is not therefore to stage some revelation of a supposed hidden truth of software, to unmask its esoteric reality, but to see what it is and what it can be coupled with: a rich seam of paradoxical conjunctions in which the speed and rationality of computation meets with its **ostensible outside**"

* What it means by a medium?
* How could you relate to this zooming in and out, or even inside out? 

Group 7 & 9.
> "we believe that paying attention to fundamental, or key, concepts from programming offers the possibility to open up new insights into aesthetics and critical theory, and new perspectives on cultural phenomena increasingly bound to computational logic. In extending the discussion, beyond formal logic to its outside, we also emphasise the usefulness of artistic practice to open up more **speculative and alternative imaginaries**. In this spirit, and in keeping with the development of software studies in Europe at least, we take inspiration from what has been referred to as *software art* (although admittedly the category was only meant as a temporary holding position).That we draw upon examples from artistic (and critical design) practices as part of our argument"

* Can you recall any artistic/design example in the class? What does it open up more speculative and alternative imaginaries? 
* What's software art? 

Group 8.
> "in order to discuss the aesthetic dimensions of code and computational processes, we incorporate artistic works that explore the material conditions of software and operations of computational processes as practical and theoretical examples. They are an intergal part of our argument in other words, as well as usefully demonstrate some of the ideas in practice and offer unexpected epistemic insights. We might add, and repeating what has already been introduced, that we are not simply interested in a **critical aesthetics** of programming but also **programming as critical aesthetics**."

* How might you understand the notion of critical aesthetics? 
* What might be programming as critical aesthetics? 


You should refine your responses through group discussion and refine it down to a short reflection with a concrete experience or example that you have encountered inside/outside of the classroom. This could be 4 sentences, but should be clear and substantive. Post on: https://padlet.com/siusoon/iaaabkoo9qey with the starting title "class06 - Group [x]" 