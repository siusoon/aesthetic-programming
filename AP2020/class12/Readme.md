## Class 12: Advanced Topic - Machine Learning

(adjusted according to the immediate introduction of online teaching)

**Messy notes:**

**Agenda:**

1. Mini-ex[10] - flowcharts short discussion + next MiniX
    - Final Project Discussion
2. Group 10 presentation: responding to one of the themes...
3. Exercise: Eliza
4. What's learning? (w/ exercise: Teachable Machine)
5. Mini-lecture: Learning Algorithms
6. ml5 in brief

---

### 1. Mini-ex[10] 

- flowcharts (individual/group; communicative/collaborative)
    - individual (communicative, visual/sound driven in order to unfold the technical processes, pay attention to words)
    - group (for work in progress - just simply need to ensure the details of smaller tasks and everyone in the group can understand it)
    
- next MiniX11 (final project draft in group): https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2020/ex

**Final Project Discussion**

![](https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/Ap2019/class07/ap.png)

- Consider how your sketch addresses one of the themes and how you use the selected text(s)
- It is good to include more than 1 text, and also include text from Software Studies (but make sure they are supportinng your focus)
- Sharing workload within the group
- Making sure everyone know the code for the final exam

Supervision schedule: https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2020 

- Due on this Sun (upload on BLACKBOARD)
- Prepare feedback (as detailed as possible, and look at the brief...and do they respond to the brief technically and aesthetically. What is your view)
- Supervision (2 groups in a row) on Tue and Wed

Timeline (where are we now)
- https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/ex/FinalProject.md 

---

### 2. Group 10 presentation 

Select a theme from the syllabus and present the required text with your own related code example and/or experience. The focus is how you make sense of what might be Aesthetic Programming)

1. Frederikke Hostrup Bering 
2. Simone 
3. Mads Marschall 
4. Røskva 

---

### 3. Exercise: Eliza

Brief background (Eliza - chatbot - AI - intelligence (human/machine) - assistants)

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/10-MachineLearning/ch10_5.png" width="600">

Group tasks (15 mins):

**Tasks1:**

Visit the Eliza Test by clicking the button 'Next step' via the link https://www.masswerk.at/elizabot/eliza_test.html. This is to see the original example given by Joseph Weizenbaum in his article on the ELIZA.

**Task2:**

Visit the work E.L.I.Z.A talking (2013) via the link https://www.masswerk.at/eliza/, and to explore and experience the conversation with your own interactions.

**Discussion thread**

Share your experience about the original conversation given by Joseph Weizenbaum and your conversation with the bot:

- How do you feel when having a conversation with ELIZA in response to the language, style, aesthetics and the social forms of interactions?
- How to you see the ability, potential and limitation of technologies to capture and structure feelings and experiences?

Post on [Padlet](https://padlet.com/siusoon/iaaabkoo9qey) with the title: Class 12 - Group X

---

### 4.  What's learning? (w/ exercise: Teachable Machine)

Brief (ML: data - model - output )

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/10-MachineLearning/ch10_2.png" width="600">

**Teachable Machine**

https://teachablemachine.withgoogle.com/v1/

This web application includes both input, learning and output. By capturing images via your own web camera, the program utilizes images as the input data. There are three training classifiers that you can play with.

**Task (individual - 10-15 mins)**
Prepare three set of gestures that can be captured by a web camera. Each gesture has to be repeatedly trained by long-pressing the colored 'TRAIN' button, and this will generate the machine learning model that is based on the captured images as input data (also called training dataset). This process is to train a computer to recognize the specific gestures/images/poses so that when there is a new image input (as test dataset), the teachable machine can classify those gestures with various confidence levels, and then predict the corresponding output results. The three default output modes (GIF, Sound, Speech) can be further modified by searching for other sets of images, sound and text.

The most simplest ways to start are:

- Train 3 different set of gestures/facial expressions, then observe the predictive results as being shown in the various output forms.

- Test the boundary of recognition or classification problems, such as having different test dataset with various marginal gestures or under different conditions such as lighting:  What **can(not)** be recognized? and **why they can(not) be recognized**?

- What happens when you just do the training with only a few images? How does this amount of training input data change the way of machine prediction?


*This exercise is to get familiar with the three components in machine learning: input, learning and output, as well as the relation between data and these components. Furthermore, this execise sets the stage of thinking about the agency of prediction and processes of machine learning.*

---

### 5. Mini-lecture: Learning Algorithms

**1. Mahine Learning to Pattern Recognization**
- Begins with Eliza 
- Sheer amounts of data 
- AI and expectation
- From pattern recognization to political operation 

<img src="https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/Ap2019/class11/ml_translate.png" width="650">

<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.thenewstack.io%2Fmedia%2F2016%2F03%2FTay-says-You-are-DUMB-Too.jpg&f=1&nofb=1" width="600">

**2. Remain skeptical**

- Aesthetic and cultural aspects around machine learning
- such as: labour practices, bias, data collection, environmental concerns, creativity/authorship, transparency
- Ways of Seeing (1972) by John Berger

![](https://i.pinimg.com/originals/7c/4c/84/7c4c842549b5989d2402d176c6312d43.jpg)

**3. 3 approaches**

**4. Supervised learning**

![](https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/Ap2019/class11/ML3.png)

    - Training dataset
    - Y=f(X) 
    - relies on classification techniques

<img src="https://user-content.gitlab-static.net/0e155d61d48bb2fdb28c25df87c629377d3c4125/687474703a2f2f616e64726561737265667367616172642e646b2f77702d636f6e74656e742f75706c6f6164732f323031372f30312f6175746f6d617469632d63656e736f72736869702e676966" width="600">

[An algorithm watching a movie trailer](https://andreasrefsgaard.dk/project/an-algorithm-watching-a-movie-trailer/) by Andreas Refsgaard

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/10-MachineLearning/ch10_7.png)

[12 Hours of ImageNet](https://thephotographersgallery.org.uk/whats-on/digital-project/exhibiting-imagenet) by Nicolas Malevé


**5. Unsupervised learning**

    - does not contain a set of labelled data
    - Clustering 

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/10-MachineLearning/ch10_4.gif" width="400">
<p>
<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/10-MachineLearning/ch10_4.png" width="600">

The clustering of images based on "cuteness" and curliness" in the workshop Anatomies of Intelligence by Joana Chicau and Jonathan Reus.

**6. Reinforcement learning**

![](https://hips.hearstapps.com/pop.h-cdn.co/assets/16/10/1024x512/landscape-1457623728-alphago2.png?resize=768:*)

    - interaction with the environment, mapping situations into actions.
    - AlphaGo 
    - sequential learning and decisions

**7. In a nutshell**

<img src="https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/Ap2019/class11/ml2.png" width="800">

What's your interest in machine learning then? 

--

### 6. ml5 in brief

ml5.js: a JavaScript framework again that can be run on a web browser like p5.js. With its objectives to make machine learning approachable for a broad audience especially beginners, it builts on top of the more complex JavaScript library called TensorFlow to provide easier access with new function calls technically. 

- try: https://learn.ml5js.org/docs/#/  (with many examples and sample code)

Other ref: Coding Train by Daniel Shiffman: https://www.youtube.com/playlist?list=PLRqwX-V7Uu6YPSwT06y_AEYTqIwbeam3y 

--

