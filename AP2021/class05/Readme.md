[[_TOC_]]

# Class 05: 8 & 9 Mar | [Auto-generator](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021#class-05-week-10-8-9-mar-auto-generator)

**Messy notes:**

## 1. Check-in 

- AR: https://twitter.com/reona396/status/1368576330946600961?s=19 by レオナ @reona396
- MiniX[4]: screenshot, runMe (link), repository (link), title, description, articulation,  reference URLs  
- [feedback](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021/ex#minix4-capture-all-individual) due tmr 
- to use the shutup and code and instructor class time -> match with your workflow 
- hold your questions (link is unable to run or other questions) and check in with your instructors.
- `userStartAudio();` 

20 ECTS > ~25 hours per week 
Oral exam's examiner: Winnie Soon & Geoff Cox

- [Proxy visit @ transmediale](https://transmediale.de/exhibition-proxy-visit-and-documentation): discord screensharing + zoom 

## 2. Peer tutoring by buddy group 

- Group 5 peer-tutoring: Randomness and noise + society/culture
- Group 6 peer-tutoring: Generativity + society/culture

## 3. Abstract / Turing machine 
- data processing/capturing vs interactivity 

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/5-AutoGenerator/ch5_1.png)

🧐 Video: Turing Machines Explained - Computerphile by Assistant Professor Mark Jago: https://www.youtube.com/watch?v=dNRDvLACg5Q&list=PLzH6n4zXuckodsatCTEuxaygCHizMS0_I&index=8

🧐 Visualization in programming: https://turingmachine.io/

Take away keywords:
- performing rules / instructions 
- self-operating system 
- states: read/write (read, write, move left/right, change state, halt/stop)

## 4. Generative / rule-based art 

<img src="https://whitneymedia.org/assets/image/822825/medium_WMAA_PROGRAMMED_06_PS_SM.jpg" width="750">

_Wall drawing #289 by Sol Le Witt _

Instructions:
1. 24 lines from the center
2. 12 lines from the midpoint of each of the sides
3. 12 lines from each corner

- write it in the form of code: https://github.com/wholepixel/solving-sol/blob/master/289/cagrimmett/index.html
- RunMe: https://editor.p5js.org/siusoon/present/2RAT_BhLt 

> The idea becomes a machine that makes art. - LeWitt

## 5. Generative / rule-based art > Entropy and emergence

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/5-AutoGenerator/ch5_8.jpg" width="750">

_Joan Truckenbrod, Entropic Tangle (1975)_

Other examples: 
- flocking: https://p5js.org/examples/simulate-flocking.html
- fractal tree: https://editor.p5js.org/marynotari/sketches/BJVsL5ylz

ideas:
- machine creativity (intentionality, control) -> questioning the centrality of human
- simulation of nature
- entropy -> ordering, predictability -> evolve over time

> "Generative art refers to any art practice where [sic] artists use a system, such as a set of natural languages, rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art." - Galanter, 2003

## 6. 10 PRINT  

BASIC programming language (Commodore 64 in the 80s): `10 PRINT CHR$(205.5+RND(1));: GOTO 10`

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch5_AutoGenerator/

Following 2 RULES:

1. Throw a dice and print a backslash half the time
2. Print a forward slash the other half of the time

```javascript
let x = 0;
let y = 0;
let spacing = 10;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
}

function draw() {
  stroke(255);
  if (random(1) < 0.5) {  
    line(x, y, x+spacing, y+spacing);
  } else {
    line(x, y+spacing, x+spacing, y);
  }
  x+=10;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
```

## 7. 🧐In-class ex

🧐 RANDOM BREAKOUT room

Try to modify the existing rules, for example:

- Can we change the size, color, and spacing of the slashes?
- Can we have outputs other than just the backward and forward slashes?

Appropriation: https://twitter.com/search?q=%2310print&src=typd 

**Your task in class is to create a sketch with a clear set of rules that operates like a modified version of 10 PRINT.Post a link of your sketch next to the pad: https://pad.vvvvvvaria.org/AP2021** (You may want to use p5 web editor: https://editor.p5js.org/)

## 8. In-class discussion

🧐 RANDOM BREAKOUT room

Drawing on the text "Randomness":

- **How** is control being implemented in 10 PRINT?
- **What** might the (un)predictability of regularity be?
- **What** is randomness to a computer?
- **Discuss** the use and the role of randomness in 10 PRINT, and more generally in the arts, including literature, and games?

## 9. MiniX[5] walk-through: A generative program | due SUN mid-night

Brief:

MiniX[5] walk-through: A generative program  (check Aesthetic Programming textbook p. 139)| due SUN 

- suggested approach: annotate the assigned readings -> experiment with the core syntax -> come up with a concept -> experiment and work towards it -> readme: writing about code (ideas + reflection with text linkage)

## 9.5 Preparation for tomorrow

1. feedback 
2. Look over Langton's ant history + the sample source code for tomorrow
  - RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch5_AutoGenerator/sketch5_1/
  - Source code: https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch5_AutoGenerator/sketch5_1/sketch.js
  - text book ch. 5 > Langton's Ant 
3. If you have time: research a bit on The game of life by the British mathematician John Conway in 1970

---
---

## 10. MiniX[4] + feedback short presentation
- [Anders Opstrup Christensen](https://gitlab.com/andersopstrupchristensen/aesthetic-programming/-/tree/master/miniX4)
- [Amalie Sofie Due Jensen](https://gitlab.com/AmalieDueJensen) 

## 11. Langton's Ant (1986) by Computer Scientist Christopher Langton

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/5-AutoGenerator/ch5_6.gif)
- Rules -> generate complex and emergence behaviors 

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch5_AutoGenerator/sketch5_1/

Source code: https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch5_AutoGenerator/sketch5_1/sketch.js

Rules: 

1. If the ant is at a white cell, it turns right 90 degrees and changes to black, then moves forward one cell as a unit.
2. If the ant is at a black cell, it turns left 90 degrees and changes to white, then moves forward one cell as a unit.

To slow down the program (demo):

- `let grid_space = 5;` in Line 2: If you change the value to 10, everything will be enlarged.

- `frameRate(20);` in Line 26: Lower the frame rate value to help slow down the program.

- `draw()` in Line 28: This function contains a for-loop where n is the ant's number of steps. If so desired you can reduce the n < 100 to n < 1 (in Line 31), i.e `for (let n = 0; n < 100; n++) {` this instructs the program to only process n steps per frame.

Logic/background:
1. grid as cell units 
2. Ant has 4 possible directions: UP, RIGHT, DOWN, LEFT 
3. Initiate ant direction: UP 
4. 2 states: color of the cell on/off, black/white 
5. increase in complexity 

Functions:
- 2-dimensional grid -> using nested for-loop => `drawGrid()
- check and perform the specific rules 
- determine the next movement `nextMove()`
- `checkEdges()`

## 12. In-class discussion and ex 

🧐 Buddy group BREAKOUT room  25 mins

Practice code reading: 

- Give yourself sometime to read and tinker with the code, as well as to observe the different stages of Langton's Ant.
- Discuss the overall functions / features
- Discuss the code line by line and map out the functions 
- Try changing some of the rules 
- Each group try to bring one question to the class 

## 13. 2d arrays

variables -> arrays -> 2d arrays 

> an array of other arrays -> expressed as array[][]

e.g declare and assign value:

- `let arr = new Array(cols);`
- `arr[i] = new Array(rows);`
- arr[][]


```javascript
function drawGrid() {
 cols = width/grid_space;
 rows = height/grid_space;
 let arr = new Array(cols);
 for (let i = 0; i < cols; i++) { //no of cols
   arr[i] = new Array(rows); //2D array
   for (let j = 0; j < rows; j++){ //no of rows
     let x = i * grid_space; //actual x coordinate
     let y = j * grid_space; //actual y coordinate
     stroke(0);
     strokeWeight(1);
     noFill();
     rect(x, y, grid_space, grid_space);
     arr[i][j] = 0;  // assign each cell with the off state + color
   }
 }
 return arr; //a function with a return value of cell's status
}
```

## 14. In-class discussion

🧐 Buddy group BREAKOUT room  25 mins

In simulating living systems — such as the complex behavior of insects — there seems to be a focus on process over outcome. Let's discuss the following questions:

- Can you think of, and describe, other systems and processes that exhibit emergent behavior?
- How would you understand autonomy in this context? To what extent do you consider the machine to be an active agent in generative systems? What are the implications for wider culture?

## 15. Other input 

![](https://media.giphy.com/media/tXlpbXfu7e2Pu/source.gif)
- [Game of life by John Conway](https://www.youtube.com/watch?v=ouipbDkwHWA): Each cell interacts with other, directly adjacent, cells, and the following transitions occur
    - Any live cell with fewer than two live neighbors dies, as if by underpopulation.
    - Any live cell with two or three live neighbors lives on to the next generation.
    - Any live cell with more than three live neighbors dies, as if by overpopulation.
    - Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.

Queer life forms: 

- diastic algorithm: [Recurrent queer imaginaries](https://digital-power.siggraph.org/piece/recurrent-queer-imaginaries/) or [Queer Motto API](https://transmediale.de/almanac/) by Helen Pritchard and Winnie Soon 
- [Digital Love Languages](https://lovelanguages.melaniehoff.com/syllabus/) by Melanie Hoff 
- love-letters (1953) by Christopher Strachey -> remade by Nick Montfort: https://nickm.com/memslam/love_letters.html
    > "You are my — Adjective — Substantive," and "My — [Adjective] — Substantive — [Adverb] — Verb — Your — [Adjective] — Substantive."

## 16.  Next week: peer-tutoring + pause week (no new syntax)

<img src="https://media.giphy.com/media/ylBc46uTBHGEg/giphy.gif">

- Guidline
- buddy group 7 & 8: https://pad.vvvvvvaria.org/AP2021

- Required reading for next - With a focus on **Aesthetic** Programming + Critical making 
  1. Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24
  2. Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28 (available free online: https://networkcultures.org/blog/publication/the-critical-makers-reader-unlearning-technology/)

- basic revisit 
- proxy exhibition + guest talk by designer Cristina Cochior 



### teacher's reflection (just some notes for myself)
- first day with the 10 print works (but online setting without much time for tinkering) 
- the manual step by step walkthrough the langton's ant is very useful, and can see the starting point and repetition
- move the student presentation to the last (can try) as they are getting tired 
- the MiniX feedback question could reuse the in-class discussion for students' reflection actually 
