[[_TOC_]]

# Aesthetic Programming 2021 @ Aarhus University

**ALL ARE ONLINE AT THE MOMENT UNTIL MARCH,2021**

**Course title:** Aesthetic Programming (20 ECTS), 2021 <br>
**Name:** Winnie Soon (wsoon@cc.au.dk)<br>
**Time/Location:** Every Mon (10.00 - 13.00) @ 1584-124 & Tue (0800-11.00) @ 5008-128H  <br>

!NB:
* **[CODE of CONDUCT](https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2021/CodeofConduct.md)** for online classes
* **Tutorial**: Every Wed & Thur 16.00-18.00, conducted by Nynne Lucca and Mads Marschall respectively
* **Friday shutup and code**: Friday 10.00-1400 for the first 6 weeks except on 26 Feb, handled by Mads Marschall and Nynne Lucca
* **Buddy group and peer-tutoring**: https://pad.vvvvvvaria.org/AP2021
* **Discord in-class chat**: https://discord.gg/GYGGyw59h9
* **Class whiteboard**: [miro white board](https://miro.com/app/board/o9J_lYKfgmA=/) (password=coding)
* **Sample code repository**: https://aesthetic-programming.gitlab.io/book/ (to be updated)
* **Weekly mini-exercises + feedback**: https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2021/ex/Readme.md
~~* Forthcoming Processing Community Day on **20 March, 2021** (tentatively - Sat full day)~~

## OUTLINE:

“[Aesthetic Programming](https://kursuskatalog.au.dk/en/course/103064/Aesthetic-programming)” provides an introduction to programming and explores the relationship between code and cultural and/or aesthetic issues related to the research field of software studies. In the course, programming practices are considered as a way to describe and conceptualise the surrounding world in formal expressions, and thus also as a way to understand structures and systems in contemporary digital culture. We will use [P5.js](https://p5js.org/) primarily, which serves as a foundation for further courses in Digital Design.

The purpose of the course is to enable the students to design and programme a piece of software, to understand programming as an aesthetic and critical action which extends beyond the practical function of the code, and to analyse, discuss and assess software code based on this understanding. The course builds on the idea of practice-based knowledge development as an important IT-academic skill from the courses Interaction and Interface Design and Co-design. The insight into how knowledge of programme code and digital aesthetic forms of expression enhances critical reflection on digital designs will be extended in the courses Platform Culture, Interaction Technologies and Design Project. The course has links to Software Studies on the same semester, and the purpose of the collaboration between these two courses is to provide specific experiences with programming as a reflected and critical practice. 

## TASKS and Expectation (prerequisite for ordinary exam - Oral exam)
1. Read all the required reading and watch all the required instructional video before coming to the class as stated in the syllabus.
2. Active participation in class/instructor discussion and exercises, and presentation of your miniX + peer-feedback (including the attendance to PCD @ Aarhus on March and the possible guest lecture/workshop)
3. Do and submit all the WEEKLY mini exercises (mostly individual with a few group works) on time
4. Provide WEEKLY peer feedback online and on time
5. Peer-tutoring in a group and with a given topic: within 20 mins in-class presentation: include a demo and conceptual articulation (more than just functional - how it works)
6. Submission of the final group project - in the form of a “readme” and a “runme” (software) package

!NB: 20 ECTS is equivalent to around 25 hours per week, including lecture and tutorial. 

!NB: Online Oral exam (8-11 Jun, 2021). Examinars: Winnie Soon and (Geoff Cox/Germán Leiva)

## Other learning support environment:
1. Weekly 2 hours tutorial/instructor session (every Wed/Thur)
2. Fri Shut up and code (10.00-14.00) for the first 6 weeks except on 26 Feb
3. Digital Design Lab - [Discord](https://discord.gg/bpRQv4EqMx) Thur-Fri 10.00-16.00 (except for week 7 and Easter)
4. (highly suggest) working closely within your buddy group

## LEARNING OUTCOMES/COMPETENCES:
1. Read and write computer code in a given programming language
2. Use programming to explore digital culture
3. Understand programming as an aesthetic and critical action which extends beyond the practical function of the code

## CLASS SCHEDULE:
#### Class 01 | Week 5 | 1 & 2 Feb: Getting Started
##### With Wed/Thur tutorial session and Fri shutup and code

**Preparation before the class:**
Post the following to the [miro white board](https://miro.com/app/board/o9J_lYKfgmA=/) (password=coding)
  - What's your programming experience? What's the programming langauage that you know e.g html, css, javascript, processing, Java, C++, etc?
  - Why do you think you need to know programming? 
  - Why is it important to know programming in the Digital Design programme?

**Required Reading**
* Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
* p5.js. p5.js | get started. Available at: https://p5js.org/get-started/ [Accessed 09 Sept 2019]
* Video: Lauren McCarthy, [Learning While making P5.JS](https://www.youtube.com/watch?v=1k3X4DLDHdc), OPENVIS Conference (2015).
* Video: Daniel Shiffman, Code! Programming with p5.js, The Coding Train. Available at https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 [Accessed 09 Sep. 2019]. (**watch 1.1**)
* Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93. (on blackbord under the Literature folder)

**Suggested Reading**
- Refer to the core text book p. 47 > Further reading

**In-class structure:**
  - Introduction
  - Why we need to learn programming?
  - Syllabus, class and learning, expectation
  - What is JavaScript and why p5.js?
  - Setting up the working environment (p5.js library + Atom code editor) 
  - Run my first p5.js program
  - Exercise in class (path, folder, directory) 
  - The Web console 
  - Reading the reference guide 
  - Git and GitLab
  - The idea of RunMe and ReadMe
  - MiniX[1]: RunMe and ReadMe (check Aesthetic Programming textbook p. 46) | due SUN mid-night
  - Peer feedback - MiniX[1] | due next Tue before class

---

#### Class 02 | Week 6 | 8 & 9 Feb: Variable Geometry
##### With Wed/Thur tutorial session and Fri shutup and code

**Required Reading**
* Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70
* Video/text: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." [Executing Practices](http://www.openhumanitiespress.org/books/titles/executing-practices/), Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. OR Femke Snelting, [Modifying the Universal](https://www.youtube.com/watch?v=ZP2bQ_4Q7DY), MedeaTV, 2016 [Video, 1 hr 15 mins].
* p5.js. p5.js | Simple Shapes. [Web] Available at: https://p5js.org/examples/hello-p5-simple-shapes.html [Accessed 09 Sep. 2019].
* Daniel Shiffman, Code! Programming with p5.js, The Coding Train [online] Available at: https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 [Accessed 09 Sep. 2019]. [**watch 1.3,1.4,2.1,2.2**]

**Suggested Reading**
- short text: Femke Snelting, "Other Geometries", transmediale journal, Issue#3, 31 October 2019, https://transmediale.de/content/other-geometries
- Refer to the core text book p. 69 > Further reading

**In-class structure:**
  - Contextualization
  - Project showcase: Multi by David Reinfurt and AIMoji by process Studio
  - Coordination + sample code walkthrough
  - Exercise in class (variable) + variables 
  - Conditional structures & relational+basic operators
  - MiniX[2] walk-through: Geometric emoji  (check Aesthetic Programming textbook p. 68)| due SUN mid-night
  - Peer feedback - MiniX[2] | due next Tue before class
  - MiniX[1] + feedback revisit: RunMe and ReadMe 

---

#### Class 03 | Week 7 | 15 & 16 Feb: Infinite Loops
##### With Wed/Thur tutorial session and Fri shutup and code

**Required Reading**
* Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
* Hans Lammerant, “How humans and machines negotiate experience of time,” in *The Techno-Galactic Guide to Software Observation*, 88-98, (2018), https://monoskop.org/log/?p=20190.
* Daniel Shiffman, [Code! Programming with p5.js on YouTube](https://www.youtube.com/watch?v=1Osb_iGDdjk), The Coding Train. (practical usage on conditional statements, loops, functions and arrays - **watch 3.1, 3.2, 3.3, 3.4, 4.1, 4.2, 5.1, 5.2, 5.3, 7.1, 7.2**)

**Suggested Reading**
- Wolfgang Ernst, “‘... Else Loop Forever’: The Untimeliness of Media,”(2009), https://www.musikundmedien.hu-berlin.de/de/medienwissenschaft/medientheorien/ernst-in-english/pdfs/medzeit-urbin-eng-ready.pdf/at_download/file 
- Refer to the core text book p. 95 > Further reading

**In-class structure:**
  - Group 1 peer tutoring: Transform & society/culture, and cover push and pop
  - Group 2 peer tutoring: loop & society/culture
  - Throbber and loops, time and temporality 
  - Exercise in class (Decode) + Sample code
  - Function + Exercise in class 
  - Transform + Exercise in class 
  - project showcase & sample code 2: Asterisk Painting + Exercise in class 
  - Arrays, Conditional statements and Loops 
  - MiniX[3] walk-through: Designing a throbber  (check Aesthetic Programming textbook p. 93) | due Sun mid-night
  - Peer feedback - MiniX[3] | due next Tue before class
  - MiniX[2] + feedback revisit: Geometric emoji 

---

#### Pause week (week 8) - to catch up 

##### NO ONLINE CLASS on WEEK 8 / 22-23 Feb
##### NO MiniX and feedback

BUT WE HAVE EXERCISES TO WORK FROM HOME (feel free to work together in a group environment so that you can also chat and socialize with others):

0. Revisit the previous class materials to catch up (be focus on the topics)
1. Use the time to practice the concept of loop, repetition and moving objects 
2. Create a small sketch with the use of loop (e.g shows 1000 lines on a screen with a particular distance each)
3. Create an animated clock to show the time now (using push, pop, rotate, etc)
4. Perhaps rework your last assignment sketch and how to make it better?
5. Think about time, loops & repetition, differences, temporality, micro and macro time and the manipulation of time in computation. How could we experiene time differently with computation?

If you have any questions, please check-in on Wed/Thur with the instructors.

##### No Shutup and code on WEEK 8 / 26 Feb

##### Tutorial will still happen on WEEK 8 / 24-25 Feb

---

#### Class 04 | Week 9 | 1 & 2 Mar: Data capture
##### With Wed/Thur tutorial session and Fri shutup and code

**Required Reading**
* Soon Winnie & Cox, Geoff, "Data capture", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 97-119
* “p5.js examples - Interactivity 1,” https://p5js.org/examples/hello-p5-interactivity-1.html.
* “p5.js examples - Interactivity 2,” https://p5js.org/examples/hello-p5-interactivity-2.html.
* “p5 DOM reference,” https://p5js.org/reference/#group-DOM.
* Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw.
* Mejias, Ulises A. and Nick Couldry. "Datafication". *Internet Policy Review* 8.4 (2019). Web. 16 Feb. 2021. <https://policyreview.info/concepts/datafication>

**Suggested Reading**
- Carolin Gerlitz and Anne Helmond, “The Like Economy: Social Buttons and the Data-Intensive Web,” New Media & Society 15, no. 8, December 1 (2013): 1348–65
- Refer to the core text book p. 118 > Further reading

**In-class structure:**
- Group 3 peer-tutoring: DOM elements & society/culture
- Group 4 peer-tutoring: CSS & society/culture  
- Contextualization
- sample code + Exercise in class (Decode)
- various captures: mouse, keyboard, audio and video 
- Exercise in class (modes of capture)
- The concept of capture 
- MiniX[4] walk-through: CAPTURE ALL  (check Aesthetic Programming textbook p. 117) | due SUN mid-night
- Peer feedback - MiniX[4] | due next Tue before class
- MiniX[3] + feedback revisit: Designing a throbber 

---

#### Class 05 | Week 10 | 8 & 9 Mar: Auto-generator
##### With Wed/Thur tutorial session and Fri shutup and code

**Required Reading**
* Soon Winnie & Cox, Geoff, "Auto-generator", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 121-142
* Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
* Daniel Shiffman, “p5.js - 2D Arrays in Javascript,” Youtube,https://www.youtube.com/watch?v=OTNpiLUSiB4.

**Suggested Reading**
- Jon, McCormack et al. “Ten Questions Concerning Generative Computer Art.” *Leonardo* 47,no. 2, 2014: 135–141.
- Refer to the core text book p. 140 > Further reading

**In-class structure:**
- Group 5 peer-tutoring: Randomness and Perlin noise + society/culture
- Group 6 peer-tutoring: Generative art + society/culture 
- Turing machine, rule-based systems/algorithmic drawing + Nature simulation 
- Project showcase: Wall drawing #289 by Sol Le Witt & Entropic Tangle (1975) by Joan Truckenbrod
- Exercise in class - 10 Print
- (Reading) Langton's Ant  
- 2D arrays and nested for-loops
- Exercise in class 
- MiniX[5] walk-through: A generative program  (check Aesthetic Programming textbook p. 139)| due SUN mid-night
- Peer feedback - MiniX[5] | due Tue before class
- MiniX[4] + feedback revisit: CAPTURE ALL 

---

#### Class 06 | Week 11 | 15 & 16 Mar: Pause week 
##### With Wed/Thur tutorial session but no shutup and code 

**Required Reading**
* Soon Winnie & Cox, Geoff, "Preface", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 12-24
* Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. *The Critical Makers Reader: (Un)Learning Technology*. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28 (available free online: https://networkcultures.org/blog/publication/the-critical-makers-reader-unlearning-technology/)

**In-class structure:**
  - Group 7 & 8 presentation
  - 11.15-12.15 CET - transmediale exhibition (theme: refusal) - field trip [proxy visit](https://transmediale.de/news/exhibition-rendering-refusal-1) @ Kunstraum and Betonhalle
  - MiniX[6] walk-through: [Revisit the past](https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2021/ex/Readme.md#minix6-revisit-the-past-individual) | due SUN mid-night 
  - Discussion on miniXs 
  - Basic Concepts revisit
  - Designer and Artist talk by [Cristina Cochior](https://randomiser.info/)
  - MiniX[5] + feedback revisit: A generative program 
  - mid way evaluation 

---

#### Class 07 | Week 12 | 22 & 23 Mar: Object abstraction 
##### With Wed/Thur tutorial session and last Fri shutup and code

**Required Reading**
* Soon Winnie & Cox, Geoff, "Object abstraction", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 143-164
* Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (on blackboard\Literature)
* “p5.js examples - Objects,” https://p5js.org/examples/objects-objects.html.
* “p5.js examples - Array of Objects,” https://p5js.org/examples/objects-array-of-objects.html.
* Daniel Shiffman, “Code! Programming with p5.js,” The Coding Train (watch: 2.3, 6.1, 6.2,6.3, 7.1, 7.2, 7.3), https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA.

**Suggested Reading**
- Refer to the core text book p. 163 > Further reading

**In-class structure:**
- Group 9 presentation: 3d objects in Aesthetic Programming
- Object abstraction: properties and behaviors 
- Project showcase: Tofu Go! by Francis Lam
- Exercise in class - Decode 
- OOP - 5 steps 
- Exercise in class 
- Game logic
- MiniX[7] walk-through: Games with objects (group)  (check Aesthetic Programming textbook p. 162)| due SUN mid-night
- Peer feedback - MiniX[7] | due Tue before class

---

#### **NO CLASS on WEEK 13 / 29-30 Mar (EASTER)**

##### **NO TUTORIAL**

--- 

#### Class 08 | Week 14 | 6 Apr: Vocable Code 
##### With Wed/Thur tutorial session
##### with Fri shutup and code: 1215-1500

**Required Reading**
* Soon Winnie & Cox, Geoff, "Vocable Code", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 165-186
* Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38.
* Allison Parrish, “Text and Type” (2019), https://creative-coding.decontextualize.com/text-and-type/.
* Daniel Shiffman, “10.2: What is JSON? Part I - p5.js Tutorial” (2017),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r.
* Daniel Shiffman, “10.2: What is JSON? Part II - p5.js Tutorial” (2017),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r

**Suggested Reading**
- Refer to the core text book p. 185 > Further reading

**In-class structure:**
- Group 10 peer-tutoring: JSON + society/culture 
- Language and Code 
- Project showcase: Vocable Code by Winnie Soon 
- Exericise in class (Decode)
- Textuality - type
- JSON 
- MiniX[8] walk-through: E-lit  (check Aesthetic Programming textbook p. 184) | due SUN mid-night
- Peer feedback - MiniX[8] | due Tue before class
- MiniX[7] + feedback revisit: Games with objects

--- 

#### Class 09 | Week 15 | 12 & 13 Apr: Que(e)ry data 
##### With Wed/Thur tutorial session
##### with Fri shutup and code: 1215-1500

**Required Reading**
* Soon Winnie & Cox, Geoff, "Que(e)ry data", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 186-210
* Daniel Shiffman, “Working with data - p5.js Tutorial,” The Coding Train (10.1, 10.4 - 10.10),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r.
* Eric Snodgrass and Winnie Soon, “API practices and paradigms: Exploring theprotocological parameters of APIs as key facilitators of sociotechnical forms of exchange],”First Monday 24, no.2 (2019), https://journals.uic.edu/ojs/index.php/fm/article/view/9553/7721.

**Suggested reading**
- Refer to the core text book p. 208 > Further reading

**In-class structure:**
- Group 11 presentation: APIs + society/culture
- Project showcase: net art generator by Cornelia Sollfrank 
- Exercise in class - nag 
- Image processing 
- Exercise: accessing web APIs (step by step)
- Data structure and que(e)ring data 
- Exercise in class (APIs)
- Load pixels 
- Bugs 
- MiniX[9] walk-through: Working with APIs (group)  (check Aesthetic Programming textbook p. 207) | due SUN mid-night
- Peer feedback - MiniX[9] | due Tue before class
- MiniX[8] + feedback revisit: E-Lit 

---

#### Class 10 | Week 16 | 19 & 20 Apr: Algorithmic procedures
##### With Wed/Thur tutorial session
##### with Fri shutup and code: 1215-1500

**Required Reading**
* Soon Winnie & Cox, Geoff, "Algorithmic procedures", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 211-226
* Taina Bucher, “The Multiplicity of Algorithims,” If...Then: Algorithmic Power andPolitics (Oxford: Oxford University Press, 2018), 19–40. (available in e-library)
* Nathan Ensmenger, “The Multiple Meanings of a Flowchart,” Information & Culture: AJournal of History 51, no.3 (2016): 321-351, Project MUSE, doi:10.1353/lac.2016.0013.

**Suggested reading**
- Refer to the core text book p. 223 > Further reading

**In-class structure:**
- Group 12 peer-tutoring: Sorting problem  
- Procedures 
- Discussion in class 
- Flowcharts 
- Exercise in class 1
- Video: * Marcus du Sautoy, “The Secret Rules of Modern Living: Algorithms,” BBC Four (2015),https://www.bbc.co.uk/programmes/p030s6b3/clips.
- Exercise in class 2 - sorting 
- Project showcase: Google Will Eat itself and The project formerly known as kindle forkbomb by Ubermorgen 
- MiniX[10] walk-through: Flowcharts (both individual and group)  (check Aesthetic Programming textbook p. 222)| due SUN mid-night
- Peer feedback - MiniX[10] | due Tue before class
- MiniX[9] + feedback revisit: Working with APIs (group)
- Revisit Final project 

---
#### Class 11 | Week 17 | 26 & 27 Apr: Machine unlearning 
##### With Wed/Thur tutorial session
##### with Fri shutup and code: 1215-1500

**Preparation before the class:** 

When you read the required reading/video for this week, keep this question in mind in which we will discuss in the class: 
> There are many things we can talk about when it comes to machine learning (or AI). Which specific aspect that interests you the most? and why? (try to substantiate your thoughts with the readings and the concrete experience that you have. - Not to just say something you feel, but substantiate and articulate it)

**Required Reading**
* Soon Winnie & Cox, Geoff, "Machine unlearning", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 227-252
* [video] Ruha Benjamin, “Are Robots Racist: Reimagining the Default Settings of Technology and Society,” lecture (2019),https://www.dropbox.com/s/j80s8kjm63erf70/Ruha%20Benjamin%20Guest%20Lecture.mp4.
* [video] Yuval Noah Harari, Audrey Tang, and Puja Ohlhaver, “To Be or Not to Be Hacked? The Future of Democracy, Work, and Identity,” RADICALxChange (2020),https://www.youtube.com/watch?v=tRVEY95cI0o.
* Geoff Cox, “Ways of Machine Seeing,” Unthinking Photography (2016),https://unthinking.photography/articles/ways-of-machine-seeing.

**Suggested reading**
- Refer to the core text book p. 248 > Further reading

**In-class structure:**
- Chatbot and Eliza 
- Exercise in class with Eliza Terminal and Eliza Test by Norbert Landsteiner 
- Between input and output 
- Exercise in class (Teachable Machines)
- Learning algorithms 
- ml5.js library
- Sample code walkthrough 
- Discussion in class 
- MiniX[11] walk-through: Draft of the final project (max 4 pages) - group  (check Aesthetic Programming textbook p. 246) | due next WED mid-night
- Whole group oral Peer feedback - MiniX[11] | due before next supervision

---

#### **NO CLASS on WEEK 18 - work on your final project draft**

##### **With Wed/Thur TUTORIAL**
##### with Fri shutup and code: 1215-1515

---

#### Class 12 | Week 19 | 10 & 11 May: SUPERVISION
##### With Wed tutorial session only (Thur is public holiday)

Venue: 5361-144

Schedule to be followed. It will be group supervision:

| Time        | Buddy Groups                                
| ------------ |:-------------------------------------------           
| Mon (10.00-10.50) | Group 7 - Group 1
| Mon (11.00-12.15) | Group 3 - Group 2 - Group 6 
| Mon (12.25-13.15) | Group 5 - Group 8                     
| Tue (09.00-09.50) | Group 12 - Group 10 
| Tue (10.00-10.50) | Group 11 - Group 9 

---
#### Class 13 | Week 20 | 18 May: FINAL PROJECT PRESENTATION 
##### NO Wed/Thur tutorial session

- 10 mins presentation with demo and 5 mins Q & A for each group. 
- **NOTE!! Final project submission date + Group project presentation **

RUNDOWN:

| Time        | Group                             
| ------------|:------------------------------------------  
| 09.00-09.05 | starting - intro   
| 09.05-09.20 | Group 9            
| 09.20-09.35 | Group 8                                 
| 09.35-09.50 | Group 10    
|BREAK                          
| 10.05-10.20 | Group 3                              
| 10.20-10.35 | Group 12     
| 10.35-10.50 | Group 11     
|BREAK
| 11.05-11.20 | Group 1                             
| 11.20-11.35 | Group 5
| 11.35-11.50 | Group 2
|BREAK
| 12.00-12.15 | Group 7 
| 12.15-12.30 | Group 6
| (LUNCH) BREAK
| 13.30-15.00 | ALL - exam logistic + final evaluation

---
<br>

## CORE TEXTBOOK:
- Soon, Winnie & Cox, Geoff. *[Aesthetic Programming: A Handbook of Software Studies](http://www.openhumanitiespress.org/books/titles/aesthetic-programming/)*. London: Open Humanities Press, 2020. (Free download)

## SKETCH INSPIRATION:
- [Daily sketch in Processing](https://twitter.com/sasj_nl) by Saskia Freeke, her talk is [here: Making Everyday - GROW 2018](https://www.youtube.com/watch?v=nBtGpEZ0-EQ&fbclid=IwAR119xLXt4nNiqpimIMWBlFHz9gJNdJyUgNwreRhIbdJMPPVx6tq7krd0ww)
- [zach lieberman](https://twitter.com/zachlieberman)
- [Creative Coding with Processing and P5.JS](https://www.facebook.com/groups/creativecodingp5/)
- [OpenProcessing](https://www.openprocessing.org/) (search with keywords)

## OTHER REFERENCES:
- [JavaScript basics](https://github.com/processing/p5.js/wiki/JavaScript-basics) by p5.js
- [NEM Programming](https://www.nemprogrammering.dk/) - provide by the student
- [Text and source code: Coding Projects with p5.js by Catherine Leung](https://cathyatseneca.gitbooks.io/coding-projects-with-p5-js/)
- [Video: Crockford on Javascript (more historical context)](https://www.youtube.com/watch?v=JxAXlJEmNMg&list=PLK2r_jmNshM9o-62zTR2toxyRlzrBsSL2)
- [A simple introduction to HTML and CSS](https://sites.google.com/sas.edu.sg/code/learn-to-code)
- McCarthy, L, Reas, C & Fry, B. *Getting Started with p5.js: Making interactive graphics in Javascript and Processing (Make)*, Maker Media, Inc, 2015.
- Shiffman, D. *Learning Processing: A Beginner’s Guide to Programming Images, Animations, and Interaction*, Morgan Kaufmann 2015 (Second Edition)
- Haverbeke,M. *[Eloquent JavaScript](https://eloquentjavascript.net/)*, 2018.
- [Video: Foundations of Programming in Javascript - p5.js Tutorial by Daniel Shiffman](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA)

## Re-examination

Prerequisites: Submit all the required all mini exercises (except miniX 10 with the flowcharts planning as flowchart is part of the requirement of a final project) and a final project but these have to be all done individually. (see the ref list: https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021/ex)

