[[_TOC_]]

# Class 06: 15 & 16 Mar | [Pause week](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021#class-06-week-11-15-16-mar-pause-week)

- with Wed/Thur instructor session
- No shutup and code on Friday

**Messy notes:**

## 1. Check-in 

- Generative program is used in a website (p5.js) http://pcdcasttest.andersvisti.dk/, work by Margrete Lodahl Rolighed and Nynne Luca, source: https://github.com/DobbeltDagger/PCD2021_PodcastPlayer/tree/master/dist/js

## 2. Peer tutoring by buddy group 

- Group 7 presentation: Refer to the text "Critical Making and Interdisciplinary learning", what does it mean by critical in critical making, and how that might relate to Aesthetic Programming? (A reflection on the assigned reading)
- Group 8 presentation: Refer to the preface of Aesthetic Programming, what's aesthetic programming? What's the relation between writing, reading and thinking, and how that can be demonstrated in any of your ex?) (A reflection on the assigned reading)

## 3. transmediale exhibition (theme: refusal)

11.15-12.15 CET - [transmediale exhibition](https://transmediale.de/projects/rendering-refusal) (theme: refusal) - field trip [proxy visit](https://transmediale.de/news/exhibition-rendering-refusal-1) @ Kunstraum and Betonhalle

- zoom link: https://zoom.us/j/98661233302?pwd=RlZRa2VWNkU3WUtHUjRTRUZHQWp4QT09 (Passcode 089560).

## 4. Mid way evaluation: 

> within your group - (https://pad.vvvvvvaria.org/AP2021_Evaluation):

0. How's online teaching/learning/environment for you? 
1. What is the most remarkable example/theme/ex/thing?
2. How do you think about the course in relation to: online/zoom setting, coding environments, peer-tutoring/presentation, weekly miniX and giving/receiving feedback, course structure, assigned/suggested readings, themes, wed+thur/shutup and code session, etc? 
3. What are the things you can do to make the course better?
4. What could be changed/experimented in the course to make it better? 
5. How can we better use our peers, and create a even better collective environment? 
6. What have you learnt in Aesthetic Programming?

---
---

## 4.5 Mid way evaluation 

https://pad.vvvvvvaria.org/AP2021_Evaluation

- socializing issues/environments (relate to motivation)
- group work + physical getting together will help (within 5)
- 15 mins (would leave 15 min)
- feedback issues (no. of feedback, what to include, what to feedback - constraints - line 36)
- instructor class (examples/inspiration) 
    - also may be already start working on tue afternoon (to better utilize the wed/thur class)
- why peer tutoring/presentation (small group discussion after)
- structure of Mon+Tue (read/code/think w/ and through code)

## 5. Aesthetic Programming & Critical Making

Programming as a method, Aesthetic Programming as a methodology. A methodology to understand and articulate computational culture from a critical-aesthetic perspective. 

🧐 What does it mean by programming/making critical work? What is critical in your miniX, and how would you articulate critical-aesthetic in programming?

**critical - site - reflection - politics and power dynamics - critical theory - imaginary - alternative - intervention** 

**Critical Making and Interdisciplinary Learning by Matt ratto & Garnet Hertz**

> "material engagements with technologies to open up and extend critical social reflection" - p. 18

> "technological artifacts embody cultural values, and that technological development can be combined wit hcultural reflectivity to build provocative objects that encourage a re-evaluation of the role of technology in culture" - p. 19

> "emphasize the value of material production itself as a site for critical reflection" - p. 19

> "critical making is about turning the relationship between technology and society from a 'matter of fact' into a matter of concern'" - p. 20 

> "the process of being critical starts by denaturalizing standard assumptions, values, and norms in order to reflect on the position and role of specific technologies within society" - p. 22

**Aesthetic Programming (Praface) by Winnie Soon and Geoff Cox:**

> "address the cultural and aesthetic dimensions of programming as a means to think and act critically" - p. 13

> "to reflect deeply on the pervasiveness of computational culture and its social and political effects - from the language of human-machine languages to abstraction of objects, datafication and recent developments in automated machine intelligence, for example." - p. 13

> "take a particular interest in power relations that are under-acknowledged, such as inequalities and injustices related to class, gender and sexuality, as well as race and the enduring legacies of colonialism." - p. 14 

> "how power differentials are implicit in code in terms of binary logic, hierarchies, naming of attributes, accessibility, and how wider societal inequalities are further reinforced and perpetuated through non-representational computation" - p. 14 

> "Aesthetic programming in this sense is considered as a practice to build things, and makeworlds, but also produce immanent critique drawing upon computer science, art, and cultural theory." - p. 15

> "Such practical “knowing” also points to the practice of “doing thinking,” embracing a plurality of ways of working with programming to explore the set of relations between writing, coding and thinking to imagine,create and propose “alternatives.” - p. 15

...

Look at the required learning outcome again for this course:

1. Read and write computer code in a given programming language
2. Use programming to explore digital culture
3. Understand programming as an aesthetic and critical action which extends beyond the practical function of the code

🧐 Discussion:

How do you see if your weekly miniX / miniX feedback has fullfilled the required outcomes? If not, what can be done further to extend your work beyond the focus on just practical/technical function of the code? Or how would you link the syntax, fundamental of programming to wider cultural phenomena?

## 6. MiniX[6] walk-through: Revisit the past | due SUN mid-night

Peer-feedback due before next class. 

[MiniX6](https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2021/ex/Readme.md#minix6-revisit-the-past-individual)


## 7. Basic 

shapes - data types - variables - arrays - transform - web console - function - conditional structures - loops (for/while) - DOM/form elements - random - rules

literacy (writing/coding) - geometry/transform (space) - loops (time) - data capture (interactivity - data processing - datafication) - auto generator (rules, control, authorship, emergence)

## 8. MiniX[5] + feedback short presentation

- [Nina Plejdrup Hansen](https://gitlab.com/NinaPlejdrup/aesthetic-programming/-/tree/master/miniX5)

## 9. Next week 

Group 9 presentation: What's 3d objects in **aesthetic programming**? (tie the notion of object to this week reading)

## 10. Guest speaker 

0930 - 10.45 - Designer and Artist talk by [Cristina Cochior](http://randomiser.info/)

