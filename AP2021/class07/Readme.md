[[_TOC_]]

# Class 07: 22 & 23 Mar | [Object abstraction](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021#class-07-week-12-22-23-mar-object-abstraction)

- with Wed/Thur instructor session
- with our last shutup and code on Friday

**Messy notes:**

## 1. check-in

- extending shutup and code for 5 more sessions :) 
- Easter is coming :) Pause/HOLIDAY lol

## 2. Object Oriented Programming 

![](https://i.pinimg.com/originals/e9/49/d3/e949d3a8a899405668d67048c39d1bdc.jpg)

- programs are organized around data, or objects, rather than functions and logic 
- a 'selected' representation of the world -> perceiving the world -> object-oriented modeling of the world is a social-technical practice (Fuller & Goffey)
- computational abstraction:

<img src="https://user-content.gitlab-static.net/c85df282925f55b2115896a7ffaba2f56e2a8cfb/68747470733a2f2f6d69726f2e6d656469756d2e636f6d2f6d61782f3537322f312a714a3750415f632d35463447565739366f4e6c3431672e6a706567" >

Source: https://medium.com/@twitu/a-dive-down-the-levels-of-abstraction-227c96c7933c

## 3. Peer tutoring: 

Group 9 presentation: What's 3d objects in aesthetic programming? (tie the notion of object to this week's reading)

## 4. Pseudo objects 

Object Abstraction in computing is about **representation**. 

Example:

**Properties**: A person with the **name** Winnie, has black **hair color**, **wears** glasses with height as 164 cm. Their **favorite color** is black and their **favorite food** is Tofu.

**Behavior**: A person can **run** from location A (Home) to location B (University).
From the above, we can construct a pseudo class that can use to create another object with the following properties and behaviors:

To put into the code planning:
|Person                  |
| ---------------------- |
| Name, HairColor, withGlasses, Height, FavoriteColor, FavoriteFood, FromLocation, ToLocation |
| run()        |


In the same token, we can "reuse" the same properties and behavior to create another "object instance" with the corresponding data values:

| Object instance 1             | Object instance 2         |
|-------------------------------|---------------------------|
| Name = Winnie                 | Name = Geoff              |
| HairColor = Black             | HairColor = Brown         |
| withGlasses = Yes             | withGlasses = Yes         |
| Height = 164 cm               | Height = 183 cm           |
| FavoriteColor = Black         | favoriteColor = Green     |
| FavoriteFood = Tofu           | FavoriteFood = Avocado    |
| FromLocation = Home           | FromLocation = University |
| ToLocation = University       | ToLocation = Home         |
| run()                         | run()                     |

## 5.  Tofu Go! by Francis Lam

<img src="https://user-content.gitlab-static.net/f9a19fb7f93c40ffb1be46a0f75b9c40ecba4771/68747470733a2f2f757365722d636f6e74656e742e6769746c61622d7374617469632e6e65742f633465343636373034333737393961323530616434383133353031383036643861363937336661352f3638373437343730336132663266373036313739366336663631363433333332333632653633363137323637366636333666366336633635363337343639373636353265363336663664326633313266333133373266333533363333333533333334326633383338333233333334333933313266363937303638366636653635333035663336333733303265373036653637">

https://www.youtube.com/watch?v=V9NirY55HfU

Tofu Go! (2018), a game developed and designed by [Francis Lam](https://dbdbking.com/) (HK/CN).

About Francis: Francis Lam is a designer, new media artist and programmer currently based in Shanghai. He was brought up and educated in Hong Kong. After his studies in Computer Science and Graphic Design, he moved to Boston and received his Master’s degree in Media Arts and Sciences from the MIT Media Lab. Francis is interested in the social aspects of interactive media and new interfaces for computer-mediated communications. His new media arts and installations have been exhibited and awarded worldwide.

In 2008, he moved to Shanghai and joined Wieden+Kennedy as the Interactive Creative Director and Head of Interactive. He was then the Chief Innovation and Technology Officer at Isobar China and leading Nowlab China, the in-house R&D unit, to bring innovative products and services to the market. Francis has created many notable digital and interactive work for clients such as Nike, Estée Lauder, Tiffany, Levi’s, Coca Cola and Unilever that drew the attention of millions of consumers in China. 

Interview: [Francis Lam](https://www.design-china.org/post/35833433475/francis-lam)

---

When tofu becomes a computational object, as in Tofu Go!, abstraction is required to capture the complexity of processes and relations, and to represent what are thought to be essential and desired properties and behaviours. In the game, tofu is designed as a simple white three-dimensional cube form with an emoticon, and the ability to move and jump. Of course in the real world tofu cannot behave in that way, but one can imagine how objects can perform differently when you program your own software, and if you love tofu as Lam does: "Tofu Go! is a game dedicated to my love for tofu and hotpot" as he puts it. 

## 6. Sample code 

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/6-ObjectAbstraction/ch6_3.gif)

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch6_ObjectAbstraction/

Source code repository: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch6_ObjectAbstraction

**Exercise**

*SPECULATION* (Together whole class)

Based on what you see/experience on the screen, describe:

* **What** are the instructions/rules for playing the game?
* Tofu is constructed as a class, and each tofu is an object instance. Can you describe the **properties** of the tofu and their **behaviors**?
* Can you describe the algorithmic procedures and sequences of the game using the following components: tofu, Pacman, keypress events, movements?

Further questions to think about:
* There is a continous new tofus moving from right to left, **what** are the conditions to trigger new tofu?  (simply speculate it)
* **How** do you check if Pacman has eaten the tofu? (what is the logic you think?)
* Under which conditions will the game end?

## 7. OOP - 5 steps (hands-on)

[🔴 video recording]

In a nutshell, an object consists of attributes/properties and actions/behaviors, and all these hold and manage data in which the data can be used and operation can be performed. 

To first construct objects in OOP, it is important to have a blueprint. A class specifies the structure of its objects' attributes and the possible behaviors/actions of its objects. Thus, class can be understood as a template and blueprint of things.  

Similar to the template that we had for a person-object, we have the following:

|Tofu                                              |
| ------------------------------------------------ |
| speed, xpos, ypos, size, toFu_rotate, emoji_size |
| move(), show()                                   |

- **(Step 1) Naming**: name your class
- **(Step 2) Properties**: What are the (varying) attributes/properties of tofu?
- **(Step 3) Behaviors**: What are the tofu's behaviors? 
- **(Step 4) Object creation and usage**
- **(Step 5) Trigger point**: Consider this holistically.

Outcome of the class live coding demo: https://editor.p5js.org/siusoon/sketches/HAYWF3gv

**(Step 1) Naming: name your class**

```javascript
class Tofu {

}
```

**(Step 2) Properties**: What are the (varying) attributes/properties of tofu?**

```javascript
class Tofu { //create a class: template/blueprint of objects with properties and behaviors
  constructor() { //initalize the objects
  this.speed = floor(random(2,5));
  this.size = floor(random(15,35));
  this.pos = new createVector(width+5,200); ////check this feature: https://p5js.org/reference/#/p5/createVector
  }
}
```

**(Step 3) Behaviors: What are the tofu's behaviors?**

```javascript
class Tofu {
  constructor() { //initalize the objects
    // something here
  }
  move() { //moving behaviors
    this.pos.x-=this.speed; //i.e, this.pos.x = this.pos.x - this.speed;
  }
  show() {
      //show Tofu as a cube by using vertex 
      //show the emoji on the Tofu surface/pane
    rect(this.pos.x, this.pos.y, this.size, this.size);
  }
}
```

**(Step 4) Object creation and usage**

```javascript 
let min_tofu = 5;  //min tofu on the screen
let tofu = [];

function setup() {
 //something here
 createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(240);
  checkTofuNum();
  showTofu();
}

function checkTofuNum() {
  if (tofu.length < min_tofu) {
    tofu.push(new Tofu());  //create new object instances -> go through the class/object
  }
}

function showTofu(){
  for (let i = 0; i <tofu.length; i++) {
    tofu[i].move();
    tofu[i].show();
  }
}

```

NB: reusability of objects

**(Step 5) Trigger point and logics 

```javascript
let min_tofu = 5;  //min tofu on the screen
let tofu = [];

function setup() {
 //something here
}

function draw() {
 //something here
 checkEating();
}

function checkEating() {
  for (let i = 0; i < tofu.length; i++){
    if (tofu[i].pos.x < 0) {
      tofu.splice(i,1);
    } 
  }
}
```

## 8. MiniX[7] walk-through: Games with objects | due SUN mid-night

MiniX[7] walk-through: Games with objects (check Aesthetic Programming textbook p. 162)| due SUN mid-night

---
---

## 9. Separate js file 

**Note:**
- How to incorporate seperate js file to better manage your overall sketch:

```javascript
//in your html file
  <script language="javascript" type="text/javascript" src="sketch.js"></script>
  <script language="javascript" type="text/javascript" src="Tofu.js"></script>
```

Why we need seperate js files? 

## 10. Exercise in class 

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch6_ObjectAbstraction/

Source code repository: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch6_ObjectAbstraction

1. Tinkering
- modify the different values to understand the function/syntax
- add more features/behaviors to the game such as 
    * add sound for each successful/fail score?
    * Any others you can think of...

2. Discussion in groups:
- Identify a game that you are familiar with, and describe the characters/objects by using the concept and vocabulary of class and object. 

> Fuller and Goffey suggest that this Object-Oriented Modelling of the world is a social-technical practice, "compressiong and abstracting relations operative at different scales of realities, composing new forms of agency." 

How would you understand this? 

## 11. Reading...

![](https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/AP2021/class07/OntologicalModelling.png)

- What it means by ontological point of view on OOP? (or engage in a practice of ontological modelling)

## 12. MiniX[6] + feedback short presentation

- [Anne Tietz](https://gitlab.com/annetietz/aesthetic-programming-2021/-/tree/master/miniX6)
- [Michael Madsen](https://gitlab.com/michael_madsen/aesthetic-programming-2.sem/-/tree/master/Minix6)

## 13. Peer Tutoring after Easter

Buddy Group 10: What's JSON and what's the structure of JSON? Can you give an example of how does it work in p5.js? Following the approach of Critical Making/Aesthetic Programming, how would you denaturalizing standard assumptions, cultural values, and norms of JSON in order to reflect on the position and role of this technology (which is the JSON standard that governs how data can be recognized/organized) within society? 

### teacher's reflection (just some notes for myself)

- talk more about sub class and inheritance 
- most miniX are nicely done for OOP 
- need to manage expectation and the time spent on this week miniX (building a totally new game requires a lot of time, including the craft of objects, logics and interactions)
