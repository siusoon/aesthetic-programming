# Weekly mini ex9: due week 15, Monday night | Working with APIs

**Objective:**
- To design and implement a program that utilizes web API(s).
- To learn to code and conceptualize a program collaboratively.
- To reflect upon the processes of data parsing via API, paying attention to registration, availability/selection/manipulation of data

**Get some inspiration here with different APIs source code**

** As APIs is really changing fast, I am unsure if all the links below are 100% working, but at least they can demonstrate the method of getting different data source.

  - [Open Weather with code example](https://www.youtube.com/watch?v=ecT42O6I_WI)
  - [Other weather API by apix with code example](https://p5js.org/examples/hello-p5-weather.html)
  - [New York times with code example](https://www.youtube.com/watch?v=IMne3LY4bks&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&index=9)
  - [Giphy images with code example](https://www.youtube.com/watch?v=mj8_w11MvH8&index=10&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r)
  - [Wikipedia API](https://www.youtube.com/watch?v=RPz75gcHj18)
  - [Twitter API and Twitter Bot with code example](http://shiffman.net/a2z/twitter-bots/) ** As far as I know Twitter has tighten the rules a lot and you need to have a solid proposal in order to get API's keys and it will take weeks to do it.
  - [Movie API with code example](https://itp.nyu.edu/classes/cc-s16/movie-api-data/)
  - [Global statistic API with code example](https://itp.nyu.edu/classes/cc-s16/inqubu-global-statistics-api/)
  - [Google map API](https://developers.google.com/maps/documentation/javascript/)
  - [Search many other kinds of API](https://www.programmableweb.com/)
  - Refer to in-class material for Google Image search (w/ source code)

**for those APIs require OAuth, you need [Node.js](https://nodejs.org/en/). See what is Node [here](https://www.youtube.com/watch?v=RF5_MPSNAtU&index=1&list=PLRqwX-V7Uu6atTSxoRiVnSuOn6JHnq2yV) for 15.1 and 15.2**

**Tasks:**
1. First, take a deep breath. This mini exercise may require sometimes to do it. This is a group mini exercise (for both runme and readme) with your study group.
2. Make sure you have read/watch the required readings/instructional videos and references for this week. (it is also a really good idea to look at the above inspirational materials)
3. This is a relatively complex exercise that requires you to
  - Design a program that utilizes at least one web API (think about what you want to say conceptually)
    - Source for a Web API
    - Understand what are the available data: the data file format and API specification
    - Decide which data fields you want to choose to explore
    - Utilize the web API and the corresponding data in your suggested program
    - Please reserve more time if you are getting data from other unfamiliar platforms, as the registration process can take a long time to do so.
4. Upload your group 'runme' to your own Gitlab account under a folder called **mini_ex9**. (Make sure your program can be run on a web browser).
5. Create a readme file (README.md) and upload to the same mini_ex9 directory (see [this](https://www.markdownguide.org/cheat-sheet) for editing the README). The readme file should contain the followings:

  - A screenshot/animated gif/video about your program (search for how to upload your chosen media and link to your gitlab)
  - A URL link to your program and run on a browser, see: https://www.staticaly.com/ or https://raw.githack.com/.
  - Who are you collaborate with / which group?
  - A title of your program
  - What is the program about? which API have you used and why?
  - Can you describe and reflect your process of making this mini exercise in terms of acquiring, processing, using and representing data? How much do you understand this provided data? How do platform providers sort the data and give you the selected data? What are the power-relationship in the chosen APIs? What's the significance of APIs in digital culture? (If possible, please make an explicit linkage with the assigned reading)
  - Try to formulate a question in relation to web APIs or querying processes that you want to investigate further when you have more time.  

6. Provide peer-feedback to 2 of your classmates on their works by creating "issues" on his/her gitlab corresponding repository. Write with the issue title "Feedback on mini_ex(?) by (YOUR FULL NAME)" (Feedback is due before next Wed tutorial class while the readme and runme are due on next Mon)

NB1: Feel Free to explore and experiment more syntax and computational structures.

NB2: The readme file should be within 8000 characters.

**mini exercise peer-feedback: guideline**
1. How does the project work in your own browser? If not, have you tried to download the whole sketch and try to tinker it? What might be the problem you guess?
2. Does the articulation/aesthetic aspect make sense to you? What have been missing in the readme? How would you use some of the elements in the assigned reading to give feedback to the person? What questions you would like to ask her/him (based on the reading)?
