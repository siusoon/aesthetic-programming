#### Class 03 | Week 8 | 19 Feb 2019: Infinite Loops
##### With Wed tutorial session and Fri shutup and code session.

### Messy notes:

#### Agenda:
1. Introduce the theme briefly + Time/Throbber
2. Peer-tutoring: push()/pop()
3. Decoding the sample code
4. Project showcase: Asterisk Painting by John P. Bell
5. arrays, loops, conditional statements, and operators
6. Walkthrough next mini-ex3: Designing a throbber differently
---
### 1. Introduce the theme briefly + Time/Throbber
<blockquote>
Loading webpages, waiting for social media feeds, streaming music, movies and other
multimedia content are mundane activities in contemporary culture. Such mundane
activity includes the involvement of network-connected devices from fixed desktop
computers to portable tablets and smart watches, all involving data transmission and
distribution across multiple sites—referred to as data. In these scenarios, data is
constantly perceived as a stream (Berry, 2011, 2012, 2013; Chun, 2017; Fuller, 2003)
indicating its characteristics of vast volume, speed of update, continuous flow and
delivery. The concept of streams[2] now characterises the Internet rather than web pages.
Data streams indicate events that are regarded as the latest and instantaneous
versions. In this way, the now that we experience through perceptible streams is
entangled with computational logics.
(Soon, forthcoming)
</blockquote>

![throbber](http://softwarestudies.projects.cavi.au.dk/images/9/99/Animatedthrobber.gif)

![progressbar](https://bash.cyberciti.biz/uploads/bashwiki/thumb/0/00/Shell-progress-bar.png/550px-Shell-progress-bar.png)

<img src="watch.png" width="500"><br>*Early wristwatches (with crystal guards) where soldiers wore them in WWI*

<img src="browser1.png" width="500"><br>*In the early 1990, developed by national center for supercompiting applications, the browser interface was designed by scientist Colleen Bushell.*

![throbber1](https://i.stack.imgur.com/dIFkT.gif)
*mosic throbber*

![browser2](browser2.png)*Different web browsers*

![throbber2](https://i.stack.imgur.com/oqMri.gif)

*Netscape web browser*

![throbber3](https://mondrian.mashable.com/uploads%252Fcard%252Fimage%252F534731%252Fc044f904-73a0-4de4-a048-601e67e79204.png%252F950x534__filters%253Aquality%252890%2529.png?signature=Mni8Y5FfSl79vGVQIoC3g3991og=&source=https%3A%2F%2Fblueprint-api-production.s3.amazonaws.com)

Buffering icons of death [see here](https://mashable.com/2017/07/12/buffering-icons-ranked/#4FSuDdS21Oqr)
=> many other different names e.g Hourglass cursor, Spinning Wait Cursor, Spinning Beach Ball, Spinning Wheel of Death, etc.

![throbber4](https://media.giphy.com/media/XXH77SsudU3HW/giphy.gif)

- The concept of *"false latency"*  -by Jason Farman, 2017 (Corporations like FB modify the speed of presentation, and try to let customers to feel things are in progress, and in a thorough manner -> expectation gap between human and technology)

- *Net Neutrality*: Internet slowdown day -> a throbber becomes a political and cultural icon
<img src="https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.asocialmediaagency.com%2Fblog%2Fwp-content%2Fuploads%2F2014%2F09%2FNetflix-slowdown.jpg&f=1">

*img src: https://www.theverge.com/2017/6/21/15816974/verizon-tumblr-net-neutrality-internet-politics-david-karp*

[Discussion forum](https://padlet.com/wsoon/aestheticprogramming)
- What is the assigned reading about? What's the argument?
- What is micro-temporal analysis?
- What is the role of 'throbber' in this article?
- How does digital technology change our perception of time?
---
### 3. Decoding the sample code
- [Run me](https://glcdn.githack.com/siusoon/aesthetic-programming/raw/master/Ap2019/class03/sketch03/index.html) (source: [sketch03-sample code](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/class03/sketch03/))
<img src="throbber.gif">

**Discuss in your group:**
- **Speculation (a strategy to decode a sketch) - based on what you see on the screen**:
  - What do you see/hear/experience on the screen?
    - what are the elements on the screen?
    - how many ellipses at the center?
    - how does it rotate? (which syntax)
    - how to make the ellipse fades out and rotates to the next position?
    - where is the image? (how to position the image?)
- **Map the elements with the code procedures:**
  - what are the elements in setup() and draw()?
  - what is translate()?
  - why push() and pop() are used here?
- **technical question/issues:**
  - what is this line means?
```javascript
let cir = 360/num*(frameCount%num);
```
*can check for The modulo operator with Golan Levin [here](https://www.youtube.com/watch?v=r5Iy3v1co0A)*

  - what does this function do?
```javascript
  function drawThrobber(num) {
    //something here
  }
```
  - To display an image, you can use the syntax createImg or loadImage, but the later one only loads the first frame

```javascript
  let loading_createimg;

  function preload() {
    loading_createimg = createImg("images/loading.gif"); //img ref: http://i.imgur.com/omGnqz7.gif
  }

  function draw() {
    loading_createimg.position(width/2-loading_createimg.width/2,20); //loads GIF - related to the width of the gif
}
```

New functions in this sketch: frameRate(), translate(), frameCount(), rotate(), modulo/%, radians, createImg(), image.position and image.size, passing argument within functions,

---
### 4. Project showcase: Asterisk Painting by John P. Bell, ported to p5.js and modified by Winnie Soon
![Asterisk Painting](Asterisk_Painting.gif)

**Artist description:** Asterisk Painting is programmed to create a series of asterisks by repeatedly printing the number of milliseconds that have passed since the painting started. If left to run by itself it will do so; however, when started on a system with other threads running in the background delays external to my artwork may make the asterisks look more like spots and the painting may resemble the work of certain other overly-litigious artists.
See [here](http://www.johnpbell.com/asterisk-painting/)

#### Running Asterisk Painting in p5.js
- Runme [here](https://glcdn.githack.com/siusoon/aesthetic-programming/raw/master/Ap2019/class03/AsteriskPainting/index.html)
- Source Code [here](https://gitlab.com/siusoon/aesthetic-programming/blob/master/Ap2019/class03/AsteriskPainting/sketch03_AsteriskPainting.js)

**New concepts**:  

**1. arrays []**   //see [here for more](https://p5js.org/examples/arrays-array.html)
    - An array is a list of data. Each piece of data in an array is identified by an index number representing its position in the array
    - an array starts with index 0

```javascript
//example
let words = [] //array -> start with 0
words[0] = "hello world";
words[1] = "what is aesthetics?";
words[2] = "something";
console.log(words[2]); //output: something
console.log(words.length); //output: 3
```

```javascript
//sample from Asterisk Painting e.g line 25-26
var xPos = [1,2,3,4,5];
var yPos = [1,2,3,4];

//xPos.length: xPos[0] = 1, xPos[1] = 2, xPos[2] = 3, xPos[3] = 4, xPos[4] = 5
//yPos.length
```

**2. conditional statements**  (if, else, else if)
- In the physical world you would say "If I am hungry then eat some food, otherwise if I am thirsty, drink some water, else I will just take a nap"

```javascript
//example in human language
if (I am hungry) { //condition > boolean expression
  eat some food;
}else if(thirsty) {
  drink some water;
}else{
  take a nap;
}
```

- About relationships between numbers. e.g 15 is greater than 20 -> false; 5 equals 5 -> true

```javascript
//sample from Asterisk Painting  :see line 63:
if(sentences >= maxSentences){  //if the existing sentence count reaches the max sentence of a asterisk painting
   move to the next one and continues;
}
```

**3. Relational operators + Logics**:

```javascript
/*
> Greater than
< Less than
<= less than or equal to
== equality
=== equality (include strict data type checking)
>= greater than or equal to
!= not equal to
!== inquality with strict type checking
*/

/*
Logical: boolean logic:
&&  //logical AND
||  //logical OR
!   //logical NOT
*/

/*
if () {
  //something here
}else if() {
  //something here
}else{
  //something here
}
*/
```
**4. loops**: for loop and while loop

- runs continuously until a specified condition is met and then exits the loop (which means loop until the condition is no longer met)
- You use a for loop because you want to repeat a section of code in an unknown/unknown number of times. (but the computer will know how many times)
- For loop: it contains
    1. a variable declaration and initialization
    2. a specificed condition
    3. loop for next round, and usually it is incremental (++) or decremental (--)
    4. The things that you want to process and compute
- While loop is to do something while the condition is true    

```javascript
/*basic structures
- What are the things that you want to loop repetitively and with a certain sequence or pattern?
- Specifically what is the condition? and When do you want to exit the loop
- What do you want to do when this condition is met?
*/
```

```javascript
//sample from Asterisk Painting:  line 38-43
/*
let i=0  => declare and initialize the variable i
i<xPos.length => The condition
i++ => move to next iteration
xPos[i] .... => what you want to do when the condition is met
*/
  for(let i=0; i<xPos.length; i++) {
    //calculate the position of each asterisk horizontally in terms of array
    xPos[i] = xPos[i] * (xDim / (xPos.length+1));
  }
  for(let i=0; i<yPos.length; i++) {  // //calculate the position of asterisk vertically in terms of array
    yPos[i] = yPos[i] * (yDim / (yPos.length+1));
  }
```
![loop](loop.png)

5. extra - time-related syntax:

```javascript
//sample from Asterisk Painting e.g line79
while(millis() < wait){}
```
- **millis()**
  - See [here](https://p5js.org/reference/#/p5/millis)
  - Returns the number of milliseconds (thousands of a second) since starting the program.
  - Normally, it uses for timing events
  - 1 second = 1000 milliseconds; 10 seconds = 10000 milliseconds

```javascript
//sample from Asterisk Painting  e.g 78-80
let wait = floor(millis() + waitTime);  //wait for next round
while(millis() < wait){}  // let the waittime pass (variable) and do nothing
milliStart = millis(); //reset the starting time
```
---
#### 6. Walkthrough next mini-ex3 and next week
- Discuss the sample code of Asterisk Painting out of class
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for the mini ex3
- Peer-tutoring: Group 2 / Respondents: Group 3, Topic: p5.dom library
