#### Class 06 | Week 11 | 12 Mar 2019: Object Orientation
##### With Wed tutorial session and Fri shutup and code session.

### Messy notes:

#### Agenda:
1. Project showcase: ToFu Go - video by Francis Lam
2. Peer-tutoring : p5 play library
3. Abstraction and OOP
4. Walkthrough OOP with the sample code
5. Walkthrough next mini-ex6: Games with Objects
---
[background video](https://loopvideos.com/V9NirY55HfU)

#### [ToFu Go](http://tofu-go.com/) by Francis Lam (HK/CN)

![tofuGo_0](http://payload326.cargocollective.com/1/17/563534/8823491/iphone0_670.png)
![tofuFo_1](https://a5.mzstatic.com/jp/r30/Purple/v4/bf/aa/0e/bfaa0e3b-a4e9-a594-a91a-29f3a15cde23/screen568x568.jpeg)

- About Francis: Francis Lam is a designer, new media artist and programmer currently based in Shanghai. He was brought up and educated in Hong Kong. After his studies in Computer Science and Graphic Design, he moved to Boston and received his Master’s degree in Media Arts and Sciences from the MIT Media Lab. Francis is interested in the social aspects of interactive media and new interfaces for computer-mediated communications. His new media arts and installations have been exhibited and awarded worldwide. In 2008, he moved to Shanghai and joined Wieden+Kennedy as the Interactive Creative Director and Head of Interactive. Francis has created many digital and interactive work for clients such as Nike, Estée Lauder, Tiffany, Levi’s, Coca Cola and Unilever that drew the attention of millions of consumers in China. He is now the Chief Innovation and Technology Officer at Isobar China and leading Nowlab, the in-house R&D unit.

- Interview:[Francis Lam](http://www.design-china.org/post/35833433475/francis-lam)

#### 2. Abstraction and OOP
![abstraction](abstraction.png)

ref: https://www.coursehero.com/file/12999936/Chapter-1/

- "capturing and focusing on the important details, while leaving out those that are less important" (Lee 2013, p. 18)

- "an abstraction is a generalization of an object which includes only those details necessary to define it and to differentiate it from other objects" (Lee 2013, p. 32)

- "it retains only those details required for accurate description and identification of the object, but excludes all other nonessential information (Jia 2003)"

e.g

|Person |
| ----------- |
| Name, HairColor, withGlasses, Height, FavoriteColor, FromLocation, ToLocation |
| run() |


![object](object0.png)

#### The theme about Object Orientation
- **Object Oriented Programming**
  - A programming paradigm

  - ![OOP](http://jofrapese.blogia.com/upload/20080627221700-dahl-nygaard.jpg)

  Background: The formal programming concept of objects was introduced in the mid-1960s with Simula 67, a major revision of Simula I, a programming language designed for discrete event simulation, created by Ole-Johan Dahl and Kristen Nygaard of the Norwegian Computing Center in Oslo. (Wikipedia)
    - "bring technical process closer to non-specialist understanding" (Fuller and Goffey 2017) => link to participatory design's utopia project that works with workers' union.
  - re-use of code and operation: "it saves time and effort to be able to write the code once and re-use it in different programs" (Fuller and Goffey 2017)

- **The concept of objects**
  - A program execution is regarded as a physical model, simulating the behavior of either a real or imaginary part of the world (Madsen et al, 1993, p.16)
  - Objects are used for representing (or modeling) physical phenomena (Madsen et al 1993, p.18)
  - Objects are computerized material (Madsen et al, 1993, p.18)
  - Objects are designed both to manage data and to carry out operations (Lee, 2013, 17)
  - "strictly defined ordering of routines and sub-routines" (Fuller and Goffey 2017)
  - "groupings of data and the methods that can be executed on that data, or stateful abstractions" (Fuller and Goffey 2017)
  - modelling behavior with and of objects. (Fuller and Goffey 2017)
<p><p><p>

  - A class specifies the structure of its objects' attributes and the possible behaviors/actions of its objects.
    - like a template and blueprint of things

  - Think of an object as a specific instance (objects are instances of classes - Lee 2013, p. 20)
    - An object holds specific values of attributes (but these values can change over time)
    - An object has behaviors, which can perform action(s) in different ways
    - In other words, "attributes and actions are combined to make up a single unified object" (Lee 2013, 17)

  - A person as an object
    - e.g1 A person with **properties** like name=winnie, haircolor=brown, wear glasses, height=165cm, favoriteColor=blue ...
    - e.g2 A person **runs** from Aarhus C to Aarhus N (run is the method- which will be defined via functions)
    - Translate into programming concepts where a program may have more than one person with the same properties but different values:

|Person |
| ----------- |
| Name, HairColor, withGlasses, Height, FavoriteColor, FromLocation, ToLocation |
| run() |


![object](object0.png)

- Discussion in Groups:

```
1. Try to discuss a game that you are familiar with and describe the characters/objects by using the concept and vocabulary of class and object.
2. What powers computational objects have? (Fuller and Goffey 2017)
3. What does it mean by object orientation as a sociotechnical practice? (Fuller and Goffery 2017)
```

#### 3. Walkthrough OOP with the sample code: class, objects, properties, arguments & behaviors

- Decoding the sample code:
<img src="tofu.gif" width="300">

  - [RUNME](https://cdn.staticaly.com/gl/siusoon/aesthetic-programming/raw/master/Ap2019/class06/sketch06/index.html)
  - [Source code](https://gitlab.com/siusoon/aesthetic-programming/blob/master/Ap2019/class06/sketch06/sketch.js)
  - [Source code - tofu class](https://gitlab.com/siusoon/aesthetic-programming/blob/master/Ap2019/class06/sketch06/Tofu.js)

- What are the objects on screen?
- What are the attributes of the toFu?
- What are the methods of the toFu?

- Coding Step by Step (1): what would be your class name?

```javascript
//create a class: template/blueprint of objects
class Tofu {

}
```
- Coding Step by Step (2): what are the attributes of the ToFu?

```javascript
class Tofu { //create a class: template/blueprint of objects with properties and behaviors
    constructor(speed, xpos, ypos, size) { //initalize the objects
    this.speed = speed;
    this.pos = new createVector(xpos, ypos);  //check this feature: https://p5js.org/reference/#/p5/createVector
    this.size = size;
    this.toFu_rotate = random(0,PI/8); //rotate in clockwise
    this.emoji_size = this.size/1.5;
    }
//something more here
}
```
- Coding Step by Step (3.1): what are the behaviors of the toFu? (for example, moving?)

```javascript
//create a class: template/blueprint of objects
class Tofu {
  constructor(getcolor, speed, xpos, ypos, size) { //initalize the objects
    // something here
  }
  move() {  //moving behaviors
    this.pos.x+=this.speed;  //i.e, this.pos.x = this.pos.x + this.speed;
  }
}
```
- Coding Step by Step (3.2): what are the behaviors of the toFu? (you need to display it on a screen)

```javascript
//create a class: template/blueprint of objects
class Tofu {
  constructor(getcolor, speed, xpos, ypos, size) { //initalize the objects
    // something here
  }
  move() {
  //something
  }
  show() { //show tofu as a cube
    push()
    translate(this.pos.x, this.pos.y);
    rotate(this.toFu_rotate);
    noStroke();
    fill(255); //front plane
    rect(0,0, this.size,this.size);
    fill(150); //top
    beginShape();
    vertex(0,0);
    vertex(0-this.size/4,0-this.size/4);
    vertex(0+this.size/0.45,0-this.size/5); //special hair style
    //vertex(0+this.size/1.5,0-this.size/4);  //no special hair style
    vertex(0+this.size, 0);
    endShape(CLOSE);
    fill(220);//side
    beginShape();
    vertex(0, 0);
    vertex(0-this.size/4,0-this.size/4);
    vertex(0-this.size/4,0+this.size/1.5);
    vertex(0,0+this.size);
    endShape(CLOSE);
    fill(80); //face
    textSize(this.emoji_size);
    text('*',0+this.size/6, 0+this.size/1.5);
    text('-',0+this.size/1.7, 0+this.size/1.9);
    text('。',0+this.size/8, 0+this.size/1.15);
    pop();
  }
}
```
- Coding Step by Step (4.1): The basic structure has been setup, next is to try to create a toFu object that can display on the screen

```javascript
let tofu = [];
let min_tofu = 5;

function setup() {
 //something here
 for (let i=0; i<=min_tofu; i++) {
   tofu[i] = new Tofu(floor(random(3,10)), 30, floor(random(height_limit+20,height_limit+115)), floor(random(30,35))); //create/construct a new object instance
 }
}
```
- Coding Step by Step (4.2): The tofu object has been created but you need to specify how you want it to perform and to show the changes over time => remember to display them on a screen.

```javascript
let tofu = [];
let min_tofu = 5;

function setup() {
 //something here
}

function draw() {
 //something here
 for (let i = 0; i <tofu.length; i++) {
   tofu[i].move();
   tofu[i].show();
   if (tofu[i].pos.x > width){
     tofu.splice(i,1); //first argument is start at which index, and the second one stands for how many
   }
 }
}
```
- Coding Step by Step (5) - thinking the holistic logic:
  - Think about if you want to create a new object when there is a trigger point e.g click on a button to create something e.g create a new tofu.
  - Think about if objects will be forever stayed on the screen, will they disappear on a screen?
  - Some other syntax would be useful in relation to objects orientation in p5js, they are push() and splice() for creating and deleting objects.

- In-class task:
  - Download the code of sketch06
  - Add one more behavior/method for the class ToFu.


---
#### extra note

![operators](operators_assignment.png)

ref: https://www.w3schools.com/js/js_operators.asp

---
#### 5. Walkthrough next mini-ex6 and next week
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for the mini ex6
- next week: Peer-tutoring: Group 5 / Respondents: Group 6, Topic: Noise + Randomness + Generative Art**
