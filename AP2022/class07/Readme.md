# Class 06:  21 & 23 Mar | Object Abstraction

**Messy notes:**

[[_TOC_]]

## 0. Announcement

- Next week - pause week + guest lecture online (Mon)
- Exam requirement: Deadline for miniX 1-7 (clear all the oustanding) by 4 Apr 2022 (0800)
  - [log](https://cryptpad.fr/sheet/#/2/sheet/edit/PuPZFgguYDg6XgK89u2ve29+/)

## 1. Object Oriented Programming

![](https://i.pinimg.com/originals/e9/49/d3/e949d3a8a899405668d67048c39d1bdc.jpg)

- What have you learnt in Software Studies class about object orientation, game studies, etc?
- programs are organized around data, or objects, rather than functions and logic
- a 'selected' representation of the world -> perceiving the world -> object-oriented modeling of the world is a social-technical practice (Fuller & Goffey)
- computational abstraction:

<img src="https://user-content.gitlab-static.net/c85df282925f55b2115896a7ffaba2f56e2a8cfb/68747470733a2f2f6d69726f2e6d656469756d2e636f6d2f6d61782f3537322f312a714a3750415f632d35463447565739366f4e6c3431672e6a706567" >

Source: https://medium.com/@twitu/a-dive-down-the-levels-of-abstraction-227c96c7933c

## 2. Pseudo objects

Object Abstraction in computing is about **representation**.

Example:

**Properties**: A person with the **name** Winnie, has black **hair color**, **wears** glasses with height as 164 cm. Their **favorite color** is black and their **favorite food** is Tofu.

**Behavior**: A person can **run** from location A (Home) to location B (University).
From the above, we can construct a pseudo class that can use to create another object with the following properties and behaviors:

To put into the code planning:
|Person                  |
| ---------------------- |
| Name, HairColor, withGlasses, Height, FavoriteColor, FavoriteFood, FromLocation, ToLocation |
| run()        |

In the same token, we can "reuse" the same properties and behavior to create another "object instance" with the corresponding data values:

| Object instance 1             | Object instance 2         |
|-------------------------------|---------------------------|
| Name = Winnie                 | Name = Geoff              |
| HairColor = Black             | HairColor = Brown         |
| withGlasses = Yes             | withGlasses = Yes         |
| Height = 164 cm               | Height = 183 cm           |
| FavoriteColor = Black         | favoriteColor = Green     |
| FavoriteFood = Tofu           | FavoriteFood = Avocado    |
| FromLocation = Home           | FromLocation = University |
| ToLocation = University       | ToLocation = Home         |
| run()                         | run()                     |


## 3. Tofu Go! by Francis Lam

<img src="https://user-content.gitlab-static.net/f9a19fb7f93c40ffb1be46a0f75b9c40ecba4771/68747470733a2f2f757365722d636f6e74656e742e6769746c61622d7374617469632e6e65742f633465343636373034333737393961323530616434383133353031383036643861363937336661352f3638373437343730336132663266373036313739366336663631363433333332333632653633363137323637366636333666366336633635363337343639373636353265363336663664326633313266333133373266333533363333333533333334326633383338333233333334333933313266363937303638366636653635333035663336333733303265373036653637">

https://www.youtube.com/watch?v=V9NirY55HfU

Tofu Go! (2018), a game developed and designed by [Francis Lam](https://dbdbking.com/) (HK/CN).

About Francis: Francis Lam is a designer, new media artist and programmer currently based in Shanghai. He was brought up and educated in Hong Kong. After his studies in Computer Science and Graphic Design, he moved to Boston and received his Master’s degree in Media Arts and Sciences from the MIT Media Lab. Francis is interested in the social aspects of interactive media and new interfaces for computer-mediated communications. His new media arts and installations have been exhibited and awarded worldwide.

In 2008, he moved to Shanghai and joined Wieden+Kennedy as the Interactive Creative Director and Head of Interactive. He was then the Chief Innovation and Technology Officer at Isobar China and leading Nowlab China, the in-house R&D unit, to bring innovative products and services to the market. Francis has created many notable digital and interactive work for clients such as Nike, Estée Lauder, Tiffany, Levi’s, Coca Cola and Unilever that drew the attention of millions of consumers in China.

Interview: [Francis Lam](https://www.design-china.org/post/35833433475/francis-lam)

---

When tofu becomes a computational object, as in Tofu Go!, abstraction is required to capture the complexity of processes and relations, and to represent what are thought to be essential and desired properties and behaviours. In the game, tofu is designed as a simple white three-dimensional cube form with an emoticon, and the ability to move and jump. Of course in the real world tofu cannot behave in that way, but one can imagine how objects can perform differently when you program your own software, and if you love tofu as Lam does: "Tofu Go! is a game dedicated to my love for tofu and hotpot" as he puts it.

## 4. Sample code

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/6-ObjectAbstraction/ch6_3.gif)

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch6_ObjectAbstraction/

Source code repository: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch6_ObjectAbstraction

**Exercise**

*SPECULATION*

Based on what you see/experience on the screen, describe:

* **What** are the instructions/rules for playing the game?
* Tofu is constructed as a class, and each tofu is an object instance. Can you describe the **properties** of the tofu and their **behaviors**?
* Can you describe the algorithmic procedures and sequences of the game using the following components: tofu, Pacman, keypress events, movements?

Further questions to think about:
* There is a continous new tofus moving from right to left, **what** are the conditions to trigger new tofu?  (simply speculate it)
* **How** do you check if Pacman has eaten the tofu? (what is the logic you think?)
* Under which conditions will the game end?

## 5. 🧐OOP - 5 steps (hands-on)

In a nutshell, an object consists of attributes/properties and actions/behaviors, and all these hold and manage data in which the data can be used and operation can be performed.

To first construct objects in OOP, it is important to have a blueprint. A class specifies the structure of its objects' attributes and the possible behaviors/actions of its objects. Thus, class can be understood as a template and blueprint of things.  

Similar to the template that we had for a person-object, we have the following:

|Tofu                                              |
| ------------------------------------------------ |
| speed, xpos, ypos, size, toFu_rotate, emoji_size |
| move(), show()                                   |

- **(Step 1) Naming**: name your class
- **(Step 2) Properties**: What are the (varying) attributes/properties of tofu?
- **(Step 3) Behaviors**: What are the tofu's behaviors?
- **(Step 4) Object creation and usage**
- **(Step 5) Trigger point**: Consider this holistically.

Outcome of the class live coding demo: https://editor.p5js.org/siusoon/sketches/HAYWF3gv

**(Step 1) Naming: name your class**

```javascript
class Tofu {

}
```

**(Step 2) Properties**: What are the (varying) attributes/properties of tofu?**

```javascript
class Tofu { //create a class: template/blueprint of objects with properties and behaviors
  constructor() { //initalize the objects
  this.speed = floor(random(2,5));
  this.size = floor(random(15,35));
  this.pos = new createVector(width+5,200); ////check this feature: https://p5js.org/reference/#/p5/createVector
  }
}
```

**(Step 3) Behaviors: What are the tofu's behaviors?**

```javascript
class Tofu {
  constructor() { //initalize the objects
  this.speed = floor(random(2,5));
  this.size = floor(random(15,35));
  this.pos = new createVector(width+5,200); ////check this feature: https://p5js.org/reference/#/p5/createVector
  }
  move() { //moving behaviors
    this.pos.x-=this.speed; //i.e, this.pos.x = this.pos.x - this.speed;
  }
  show() {
      //show Tofu as a cube by using vertex
      //show the emoji on the Tofu surface/pane
    rect(this.pos.x, this.pos.y, this.size, this.size);
  }
}
```

**(Step 4) Object creation and usage**

```javascript
let min_tofu = 5;  //min tofu on the screen
let tofu = [];

function setup() {
 //something here
 createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(240);
  checkTofuNum();
  showTofu();
}

function checkTofuNum() {
  if (tofu.length < min_tofu) {
    tofu.push(new Tofu());  //create new object instances -> go through the class/object
  }
}

function showTofu(){
  for (let i = 0; i <tofu.length; i++) {
    tofu[i].move();
    tofu[i].show();
  }
}

```

NB: reusability of objects

**(Step 5) Trigger point and logics

```javascript
let min_tofu = 5;  //min tofu on the screen
let tofu = [];

function setup() {
 //something here
}

function draw() {
 //something here
 checkEating();
}

function checkEating() {
  for (let i = 0; i < tofu.length; i++){
    if (tofu[i].pos.x < 0) {
      tofu.splice(i,1);
    }
  }
}
```

## 7. Walkthrough MiniX[6] - Games with objects
- See [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - BRIEF
  - FEEDBACK  

---
---

## 7.5 Note

[Guest lecture on Monday](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022#class-08-week-13-28-30-mar-pause-week-revisit-the-past-mini-x) (on Zoom) - Jazmin Morris

## 8. Revisit mid way evaluation

- 2022 mid way: https://pad.vvvvvvaria.org/AP2022_midway
- 2021 students advice: https://pad.vvvvvvaria.org/AP2021 

points:
- Last time: 20 ECTS = 28 hours per week & purpose of feedback 
- attendance problems: zoom / corona 
- reduction of word counts - characters (readme and feedback)
- the feeling of overwhelming (not the workload but peer pressure - but also inspiring) -> see it as a site of learning 
- pause weeks (more are coming)
- individual presentation vs peer presentation within a smaller group (will try today)

Last note: 
Deadline for miniX [1-7]: 4 Apr 2022, 0800.

## 9. Revisit the 5 steps of OOP 

- other class/object relations e.g class library

 mic = new p5.AudioIn();

 ctracker = new clm.tracker();

 |Tofu                                              |
| ------------------------------------------------ |
| speed, xpos, ypos, size|
| move(), show()                                   |

- **(Step 1) Naming**: name your class
- **(Step 2) Properties**: What are the (varying) attributes/properties of tofu?
- **(Step 3) Behaviors**: What are the tofu's behaviors?
- **(Step 4) Object creation and usage**
- **(Step 5) Trigger point**: Consider this holistically.

Outcome of the class live coding demo: https://editor.p5js.org/siusoon/sketches/HAYWF3gv

## 10.  Separate js file

**Note:**

- How to incorporate seperate js file to better manage your overall sketch:

```javascript
//in your html file
  <script language="javascript" type="text/javascript" src="sketch.js"></script>
  <script language="javascript" type="text/javascript" src="Tofu.js"></script>
```

Why do we need seperate js files?

## 11. 🧐Exercise in class

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch6_ObjectAbstraction/

Source code repository: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch6_ObjectAbstraction

1. Tinkering
- modify the different values to understand the function/syntax
- add more features/behaviors to the game such as
    * add sound for each successful/fail score?
    * Any others you can think of...

## 11.5 🧐 Discussion in groups:

- Identify a game that you are familiar with, and describe the characters/objects by using the concept and vocabulary of class and object.

> Fuller and Goffey suggest that this Object-Oriented Modelling of the world is a social-technical practice, "compressiong and abstracting relations operative at different scales of realities, composing new forms of agency."

How would you understand this?

## 12. Reading...

![](https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/AP2021/class07/OntologicalModelling.png)

- What it means by ontological point of view on OOP? (or engage in a practice of ontological modelling)

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017)

NB: [Ontology vs Epistemology](https://pediaa.com/wp-content/uploads/2016/11/Difference-Between-Ontology-and-Epistemology-infographic.png)

## 13. ⏺️Presentation by students on MiniX [5]

![](https://media.giphy.com/media/l0MYS1WzKiLlEeQJq/giphy.gif)

0. Group of 4
1. Present to one another: 5 mins each
2. Discuss the ideas and questions together
