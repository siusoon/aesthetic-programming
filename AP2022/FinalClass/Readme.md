# Final Class: 16 May
**Messy notes:**

[[_TOC_]]

## 0. 8 groups presentation

## 1. Exam prep

### 1.1 About the course

The purpose of the course is to enable the students to **design and programme a piece of software, to understand programming as an aesthetic and critical action which extends beyond the practical function of the code, and to analyse, discuss and assess software code based on this understanding.**

The course provides an introduction to programming and explores the relationship between code and cultural and/or aesthetic issues related to the research field of software studies. In the course, **programming practices are considered as a way to describe and conceptualise the surrounding world in formal expressions, and thus also as a way to understand structures and systems in contemporary digital culture.**

The course builds on the idea of practice-based knowledge development as an important IT-academic skill from the courses Interaction and Interface Design and Co-design. The insight into how knowledge of programme code and digital aesthetic forms of expression enhances critical reflection on digital designs will be extended in the courses Platform Culture, Interaction Technologies and Design Project. The course has links to Software Studies on the same semester, and the purpose of the collaboration between these two courses is to provide specific experiences with programming as a reflected and critical practice.

https://kursuskatalog.au.dk/en/course/110864/Aesthetic-programming

### 1.2 Objectives

**Knowledge:**

- Demonstrate insight into and reflect on programming as a practice that allows you to explore, think about and understand contemporary digital culture
- Demonstrate insight into programming and programming principles by writing, reading and processing code

**Skills:**

- Write, read and process code, including communicating basic programming concepts
- Analyse and asses their own and others’ attempts at expressing aesthetic issues in programme code

**Competences:**

- Read and write computer code in a given programming language
- Use programming to explore digital culture
- Understand programming as an aesthetic and critical action which extends beyond the practical function of the code

### 1.3 Oral Exam

- Start with a 4 mins presentation based on the pre-assigned question - related to the final project
  - QUESTION: **Draw a concrete technical example (a small block of code: 5-10 lines) from your final project and discuss any one of the aesthetic aspects of code that informs your critical reflection on digital design/culture.**
- When you are done with the presentation on the pre-assigned question, the examiners will follow by asking you to present one of the miniXs (except miniX[11]) in 4 mins (with a demo of your RunMe)
  - A really good tip would be to prepare something specific to say about on each miniX (based on the readings, themes, and RunMe). You are welcome to show slides, code snippets, your mini ex and other experiments, or other forms that you think could help articulate your thoughts within the realm of Aesthetic Programming.
- 12 mins discussion

Pay attention:
- time
- select/highlight the specific area carefully (no way for you to present the whole miniX or whole final project - time is not enough)
- technical and discursive elements
- show how you understand the curriculum and your insights
- Make sure you know the code in details for every miniX and the final project (individual and group work)

### 1.4 The Grading scale (12 points)

https://international.au.dk/life/studentscomingtoau/student-life/studying-at-aarhus-university/exam-and-grading/

## 2. How to use Aesthetic Programming in your DD education program

- Prototyping tools
- As a methodology: embrace both formal and disursive meanings (Critical Technical Practice)
- Read/write/run/think/design/contextualize with and about code/program as methods
- Relationship with design and computational culture

Quotes from Aesthetic Programming:

The argument the book follows is that computational culture is not just a trendy topic to study to improve problem-solving and analytical skills, or a way to understand more about what is happening with computational processes, but **is a means to engage with programming to question existing technological paradigms and further create changes in the technical system**. We therefore **consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means of understanding some of the complex procedures that underwrite and constitute our lived realities, in order to act upon those realities**.

we have been working with fundamental programming concepts, such as geometry and object abstraction; loops and temporality; data and datafication; variables, functions, and their naming, as well as data capturing, processing and automation, as the starting point for further aesthetic reflection whereby the technical functions set the groundwork for further understanding of how cultural phenomena are constructed and operationalized.

**Aesthetic programming in this sense is considered as a practice to build things, and make worlds, but also produce immanent critique drawing upon computer science, art, and cultural theory**. From another direction, this comes close to Philip Agre’s notion of “critical technical practice,” with its bringing together of formal technical logic and discursive cultural meaning. In other words, this approach necessitates a practical understanding and knowledge of programming to underpin critical understanding of techno-cultural systems, grounded on levels of expertise in both fields (as in the case of Wendy Hui Kyong Chun or Yuk Hui, for example, who have transdisciplinary expertise).

## 3. Aesthetic Programming for futures

- Code and Share website: https://codeandshare.net/
- IG/Facebook: https://pcdaarhus.net/about/
- Add to email list: andersvisti@gmail.com
- Next event: 11 Jun afteroon (place to be confirmed): sharing of software projects

## 4. what would you recommend for the next cohort?

- AP2021: https://pad.vvvvvvaria.org/AP2021
- AP2022: https://pad.vvvvvvaria.org/AP2022

## 5. Final evaluation

- "It's Difficult" 
- Machine Learning
- Fri shut up and code 
- Weekly feedback on time vs Covid/Sickness/others vs organization
