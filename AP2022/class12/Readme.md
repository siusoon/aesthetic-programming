# Class 12: 4 May | Studio time

**Messy notes:**

[[_TOC_]]

## 0. Plan for coming two weeks

## 1. Group presentation on miniX[10]

## 2. Group presentation with a 5 mins pitch of your final project

- What's the theme that you want to address?
- What would be the readings that you will use?
- What is the software about?
- If you have - walkthrough your ideas via a flowchart
- What are the challenges now?

## 3. MiniX[11] - Draft Final project (group)

- Due on 8 May (draft)

## 4. Thur and Fri (instructor session)

## 5. Studio time
