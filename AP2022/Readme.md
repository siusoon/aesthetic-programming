[[_TOC_]]

# Aesthetic Programming 2022 @ Aarhus University

**Course title:** Aesthetic Programming (20 ECTS), 2022 <br>
**Name:** Winnie Soon (wsoon@cc.au.dk)<br>
**Time/Location:** Every Mon (08.00-11.00) @ 5361-144 & Wed (11.00-14.00) @ 5361-135 <br>

**Note:**
- Tutorial: Every Thur: 1400-1600; 1600-1800 @ 5008-135, conducted by Jakob Stougaard Wang and Jonas Hegelund Rasmussen
- Friday shutup and code-camps: 13.00-16.00 @ Adorno-140 (except week 8),5008-131, 5361-144 (10 sessions in total)
- Discord in-class chat: ?
- Sample code repository: https://aesthetic-programming.gitlab.io/book/
- Weekly mini-exercises + feedback: https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex
- Class notes: https://pad.vvvvvvaria.org/AP2022

## OUTLINE:

“[Aesthetic Programming](https://kursuskatalog.au.dk/en/course/110864/Aesthetic-programming)” provides an introduction to programming and explores the relationship between code and cultural and/or aesthetic issues related to the research field of software studies. In the course, programming practices are considered as a way to describe and conceptualise the surrounding world in formal expressions, and thus also as a way to understand structures and systems in contemporary digital culture. We will use [P5.js](https://p5js.org/) primarily, which serves as a foundation for further courses in Digital Design.

The purpose of the course is to enable the students to design and programme a piece of software, to understand programming as an aesthetic and critical action which extends beyond the practical function of the code, and to analyse, discuss and assess software code based on this understanding. The course builds on the idea of practice-based knowledge development as an important IT-academic skill from the courses Interaction and Interface Design and Co-design. The insight into how knowledge of programme code and digital aesthetic forms of expression enhances critical reflection on digital designs will be extended in the courses Platform Culture, Interaction Technologies and Design Project. The course has links to Software Studies on the same semester, and the purpose of the collaboration between these two courses is to provide specific experiences with programming as a reflected and critical practice.

## TASKS and Expectation (prerequisite for taking the ordinary exam - Oral exam)
1. Read the required reading and instructional video before coming to the class as stated in the syllabus.
2. Active participation in class/instructor discussion and exercises, and presentation of your miniX + peer-feedback. Ready to engage in activities, and contribute to the class through discusson, respectful and attentive participation
3. Submit all the WEEKLY mini exercises (mostly individual with a few group works) in a timely manner
4. Provide WEEKLY [peer feedback](https://cryptpad.fr/sheet/#/2/sheet/edit/PuPZFgguYDg6XgK89u2ve29+/) online in a timely manner
5. Submission of the final group project and participate in the final group presentation- in the form of a “readme” and a “runme” (software) package

!NB: 20 ECTS is equivalent to around 25 hours per week, including lecture and tutorial.

!NB: Oral exam (14-16 Jun, 2022). Examinars: Winnie Soon and Jussi Parikka

## Oral Exam (Individual - will be discussed on the last class 16 May)
- 4 mins presentation based on the pre-assigned question - related to the final project (to be provided on 16 May)
- 4 mins presentation on one of the miniX except miniX[11] (will be asked by the examiner)
- 12 mins discussion

## Other learning support environment:
1. Weekly 2 hours tutorial/instructor session (every Thur)
2. Fri Shut up and code-camps (13.00-16.00) - 10 sessions
3. Digital Design Lab - [Discord](https://discord.gg/bpRQv4EqMx) Thur-Fri 10.00-16.00 (except for week 7 and Easter)
4. (highly suggest) working closely with your 🤸underground buddy group🤸 - take your initiative

## LEARNING OUTCOMES/COMPETENCES:
1. Read and write computer code in a given programming language
2. Use programming to explore digital culture
3. Understand programming as an aesthetic and critical action which extends beyond the practical function of the code

## CLASS PROTOCOL:
1. 🔥Code of conduct (from p5.js' [community statement](https://github.com/processing/p5.js/blob/main/CODE_OF_CONDUCT.md#p5js-code-of-conduct)): We are a community of, and in solidarity with, people from every gender identity and expression, sexual orientation, race, ethnicity, language, neuro-type, size, ability, class, religion, culture, subculture, political opinion, age, skill level, occupation, and background.
3. 🔥Use paper: Bring papers or sketch book to class, and this will help for paper prototyping, structuring your thoughts and understanding.
4. 🔥Two before me policy: When you encounter a bug in your program, you are first required to seek help from two of your peers before coming to the educator for assistance. The rationale behind is to practice how to ask and break down your question, as well as facilitating peer learning.
5. 🔥Submit your assignment and feedback in a timely manner.

## CLASS SCHEDULE:

#### 💥Class 01 | Week 6 | 7 & 9 Feb: Getting Started (Writing, Coding and Literacy)
##### With Thur tutorial session and Fri shutup and code @ Adorno-140

**Preparation before the class:**

Prepare the following, and we will discuss them in the class:
1. What's your programming experience?
2. What's the programming langauage that you know e.g html, css, javascript, processing, Java, C++, etc?
3. Why do you think you need to know programming in Digital Design Programme?
4. Beyond the STEM approach, how could we imagine learning to program otherwise?

**Required Reading**

For Mon:
* Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 13-24
* Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93. (on BrightSpace under the Literature folder)
* Video: Lauren McCarthy, [Learning While making P5.JS](https://www.youtube.com/watch?v=1k3X4DLDHdc), OPENVIS Conference (2015).

For Wed:
* Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
* Watch all the 5 videos on Brightspace (1.1-1.5) regarding web browsers, code editor atom, p5.js library, GitLab and Markdown.

**Messy note:**
- MiniX[1]: RunMe and ReadMe & Peer feedback - MiniX[1] [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)

---

#### 💥Class 02 | Week 7 | 14 & 16 Feb: Variable Geometry
##### With Thur tutorial session and Fri shutup and code @ Adorno-140

**Required Reading**
* Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70
* Video/text: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." [Executing Practices](http://www.openhumanitiespress.org/books/titles/executing-practices/), Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. OR Femke Snelting, [Modifying the Universal](https://www.youtube.com/watch?v=ZP2bQ_4Q7DY), MedeaTV, 2016 [Video, 1 hr 15 mins].
* Daniel Shiffman, Code! Programming with p5.js, The Coding Train [online] Available at: https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 [Accessed 09 Sep. 2019]. [**watch 1.3,1.4,2.1,2.2**]

**Suggested Reading**
- short text: Femke Snelting, "Other Geometries", transmediale journal, Issue#3, 31 October 2019, https://transmediale.de/content/other-geometries
- Crystal Abidin and Joel Gn, eds., "Histories and Cultures of Emoji Vernaculars," *First Monday* 23, no. 9, September (2018), <https://firstmonday.org/ojs/index.php/fm/issue/view/607>.
- Christian Ulrik Andersen and Geoff Cox, eds., *A Peer-Reviewed Journal About Machine Feeling* 8, no. 1 (2019), <https://aprja.net//issue/view/8133>.
- Derek Robinson, "Variables," in Matthew Fuller, ed., *Software Studies: A Lexicon* (Cambridge, MA: MIT Press, 2008).
- Xin Xin, "1.2 2D Primitives - p5.js Tutorial," (2020) <https://www.youtube.com/watch?v=hISICBkFa4Q>.
- Xin Xin, "1.3 Color and blendMode() - p5.js Tutorial," (2020) <https://www.youtube.com/watch?v=fTEvHLLwSBE>.

**Messy note:**
- MiniX[2]: Geometric emoji  & Peer feedback, [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)

---

#### 💥Class 03 | Week 8 | 21 & 23 Feb: Infinite Loop
##### With Thur tutorial session and **NO** Fri shutup and code but Guest Speaker on Fri

**25 Feb (FRI) GUEST SPEAKER: 1400-1600 by Alessandro Ludovico**

Topic: Curating and archiving media art, processual strategies by Alessandro Ludovico

**Required Reading**
* Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
* Daniel Shiffman, Courses 3.1, 3.3, 5.2, 9.1, 7.1, 7.2 *Code! Programming with p5.js*, (2018) <https://thecodingtrain.com/beginners/p5js/index.html>. (Includes practical usage on conditional statements, loops, functions, transformation, arrays)
* Hans Lammerant, “How humans and machines negotiate experience of time,” in *The Techno-Galactic Guide to Software Observation*, 88-98, (2018), https://monoskop.org/log/?p=20190.

**Suggested Reading**
* Wolfgang Ernst, “‘... Else Loop Forever’: The Untimeliness of Media,”(2009), https://www.musikundmedien.hu-berlin.de/de/medienwissenschaft/medientheorien/ernst-in-english/pdfs/medzeit-urbin-eng-ready.pdf
* Daniel Shiffman, [Code! Programming with p5.js](https://thecodingtrain.com/beginners/p5js/index.html), The Coding Train. (practical usage on conditional statements, loops, functions and arrays - watch 3.2, 3.4, 4.1, 4.2, 5.1, 5.3)
* Xin Xin, "1.4 translate(), rotate(), push(), pop() - p5.js Tutorial," (2020) <https://www.youtube.com/watch?v=maTfm84mLbo&list=PLT233rQkMw761t_nQ_6GkejNT1g3Ew4PU&index=4>.
* Wolfgang Ernst, *Chronopoetics: The Temporal Being and Operativity of Technological Media* (London: Rowman & Littlefield International, 2016), 63-95.
* Winnie Soon, “Throbber: Executing Micro-temporal Streams,” *Computational Culture* 7, October 21 (2019), http://computationalculture.net/throbber-executing-micro-temporal-streams/.
* Derek Robinson, "Function," in Fuller, ed., *Software Studies*.

---

#### 💥Class 04 | Week 9 | 28 Feb & 2 Mar: Catch up week
##### With Thur tutorial session and Fri shutup and code @ Adorno-140

- No new readings but pls re-read this: Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 13-24 (we will discuss in the class)
- Pls catch up the past readings, instructional videos, etc
- For the class preparation: Please bring one software artwork/critical design work (requires programming in particular) that interests you.

**Messy note:**
- MiniX[3]: Designing a throbber & Peer feedback, [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)

---

#### 💥Class 05 | Week 10 | 7 & 9 Mar: Data Capture
##### With Thur tutorial session and Fri shutup and code @ Adorno-140

**Required Reading**
* Soon Winnie & Cox, Geoff, "Data capture", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 97-119
* “p5.js examples - Interactivity 1,” https://p5js.org/examples/hello-p5-interactivity-1.html.
* “p5.js examples - Interactivity 2,” https://p5js.org/examples/hello-p5-interactivity-2.html.
* “p5 DOM reference,” https://p5js.org/reference/#group-DOM.
* Mejias, Ulises A. and Nick Couldry. "Datafication". *Internet Policy Review* 8.4 (2019). Web. 16 Feb. 2021. <https://policyreview.info/concepts/datafication>

**Suggested Reading**
* Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw.
* Søren Pold, “Button,” in Fuller, ed., Software Studies.
* Carolin Gerlitz and Anne Helmond, “The Like Economy: Social Buttons and the Data-Intensive Web,” New Media & Society 15, no. 8, December 1 (2013): 1348–65.
* Christian Ulrik Andersen and Geoff Cox, eds., A Peer-Reviewed Journal About Datafied Research 4, no. 1 (2015), https://aprja.net//issue/view/8402.
* Audun M. Øygard, “clmtrackr - Face tracking JavaScript library,” https://github.com/auduno/clmtrackr.
* Daniel Shiffman, HTML / CSS/DOM - p5.js Tutorial (2017), https://www.youtube.com/playlist?list=PLRqwX-V7Uu6bI1SlcCRfLH79HZrFAtBvX.
* Tiziana Terranova, “Red Stack Attack! Algorithms, Capital and the Automation of the Common,” EuroNomade (2014), http://www.euronomade.info/?p=2268.

**In-class structure:**
- MiniX[4]: Capture All & Peer feedback, [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)

---

#### 💥Class 06 | Week 11 | 14 & 16 Mar: Auto-Generator
##### With Thur tutorial session and Fri shutup and code @ Adorno-140

**Required Reading**
* Soon Winnie & Cox, Geoff, "Auto-generator", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 121-142
* Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
* Daniel Shiffman, “p5.js - 2D Arrays in Javascript,” Youtube,https://www.youtube.com/watch?v=OTNpiLUSiB4.

**Suggested Reading**
- Jon, McCormack et al. “Ten Questions Concerning Generative Computer Art.” *Leonardo* 47,no. 2, 2014: 135–141.
- Philip Galanter, “Generative Art Theory,” in Christiane Paul, ed., A Companion to Digital Art (Oxford: Blackwell, 2016), http://cmuems.com/2016/60212/resources/galanter_generative.pdf.
- “How to Draw with Code | Casey Reas,” Youtube video, 6:07, posted by Creators, June 25 (2012), https://www.youtube.com/watch?v=_8DMEHxOLQE.
- Daniel Shiffman, “p5.js Coding Challenge #14: Fractal Trees - Recursive,” https://www.youtube.com/watch?v=0jjeOYMjmDU.
- Daniel Shiffman, “p5.js Coding Challenge #76: Recursion,” https://www.youtube.com/watch?v=jPsZwrV9ld0. - Daniel Shiffman, “noise() vs random() - Perlin Noise and p5.js Tutorial,” https://www.youtube.com/watch?v=YcdldZ1E9gU.

**In-class structure:**
- MiniX[5] walk-through: A generative program  (check Aesthetic Programming textbook p. 139)
- Peer feedback - MiniX[5]

---

#### 💥Class 07 | Week 12 | 21 & 23 Mar: Object Abstraction
##### With Thur tutorial session and Fri shutup and code @ 5008-131

**Required Reading**
* Soon Winnie & Cox, Geoff, "Object abstraction", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 143-164
* Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (on BrightSpace\Literature)
* “p5.js examples - Objects,” https://p5js.org/examples/objects-objects.html.
* “p5.js examples - Array of Objects,” https://p5js.org/examples/objects-array-of-objects.html.
* Daniel Shiffman, “Code! Programming with p5.js,” The Coding Train (watch: 2.3, 6.1, 6.2,6.3, 7.1, 7.2, 7.3), https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA.

**Suggested Reading**
- Cecile Crutzen and Erna Kotkamp, “Object Orientation,” in Fuller, ed., Software Studies, 200-207.
- Roger Y. Lee, “Object-Oriented Concepts,” in Software Engineering: A Hands-On Approach (Springer, 2013), 17-24, 35-37.
- Daniel Shiffman, “16.17 Inheritance in JavaScript - Topics of JavaScript/ES6,” https://www.youtube.com/watch?v=MfxBfRD0FVU&feature=youtu.be&fbclid=IwAR14JwOuRnCXYUIKV7DxML3ORwPIttOPPKhqTCKehbq4EcxbtdZDXJDr4b0.
- Andrew P. Black, “Object-Oriented Programming: Some history, and challenges for the next fifty years” (2013), https://arxiv.org/abs/1303.0427.

**In-class structure:**
- MiniX[6] walk-through: Games with objects (check Aesthetic Programming textbook p. 162)
- Peer feedback - MiniX[6]

---

#### 💥Class 08 | Week 13 | 28 & 30 Mar Pause Week: Revisit the past mini X

##### With Thur tutorial session and Fri shutup and code @ Adorno-140

Guest lecture/Workshop by Jazmin Morris on 28 Mar (online/zoom)

**Title:** Inbetween Binary
[My Journey through UAL](https://www.youtube.com/watch?v=sSPfzNa60JI) - Video
[Art, Avatars & AI](https://www.machineunlearning.co.uk/episodes/jazminmorris) - Podcast

**Required Reading**
NO

**In-class structure:**
  - Forming groups
  - MiniX[7] walk-through: [Revisit the past] - no peer feedback
  - Exam arrangement

---

#### 💥Class 09 | Week 14 | 4 & 6 Apr: Vocable Code
##### With Thur tutorial session and Fri shutup and code @ Adorno-140

**Required Reading**
* Soon Winnie & Cox, Geoff, "Vocable Code", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 165-186
* Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38.
* Allison Parrish, “Text and Type” (2019), https://creative-coding.decontextualize.com/text-and-type/.
* Daniel Shiffman, “10.2: What is JSON? Part I - p5.js Tutorial” (2017),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r.
* Daniel Shiffman, “10.2: What is JSON? Part II - p5.js Tutorial” (2017),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r

**Suggested Reading**
- Geoff Cox, Alex McLean, and Adrian Ward, “The Aesthetics of Generative Code,” Proceedings of Generative Art Conference, Milan (2001).
- Liz W. Faber, The Computer’s Voice: From Star Trek to Siri (Minneapolis, MN: University of Minnesota Press, 2020).
- Rita Raley, “Interferences: Net.Writing and the Practice of Codework,” Electronic Book Review (2002), http://electronicbookreview.com/essay/interferences-net-writing-and-the-practice-of-codework/.
- Margaret Rhee, “Reflecting on Robots, Love, and Poetry,” XRDS: Crossroads 24, no. 2, December (2017): 44–46, https://dl.acm.org/doi/pdf/10.1145/3155126?download=true.
- Douglas Crockford, “The application/json Media Type for JavaScript Object Notation (JSON).” RFC 4627 (2006), https://www.ietf.org/rfc/rfc4627.txt.

**In-class structure:**
- MiniX[8] walk-through: E-lit (group)  (check Aesthetic Programming textbook p. 184)
- Group Peer feedback - MiniX[8]

---

**HOLIDAY WEEK 15**

---

#### 💥Class 10 | Week 16 | 20 Apr ONLY: Algorithmic Procedures
##### With Thur tutorial session and Fri shutup and code @ 5361-144

**Required Reading**
* Soon Winnie & Cox, Geoff, "Algorithmic procedures", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 211-226
* Taina Bucher, “The Multiplicity of Algorithims,” If...Then: Algorithmic Power andPolitics (Oxford: Oxford University Press, 2018), 19–40. (available in e-library)

**Suggested reading**
- Nathan Ensmenger, “The Multiple Meanings of a Flowchart,” Information & Culture: AJournal of History 51, no.3 (2016): 321-351, Project MUSE, doi:10.1353/lac.2016.0013.
- Ed Finn, “What is an Algorithm,” in What Algorithms Want (Cambridge, MA: MIT Press, 2017), 15-56. Andrew Goffey, “Algorithm,” in Fuller, ed., Software Studies, 15-20.
- Marcus du Sautoy, “The Secret Rules of Modern Living: Algorithms,” BBC Four (2015), https://www.bbc.co.uk/programmes/p030s6b3/clips.

**In-class structure:**
- MiniX[9] walk-through: Flowcharts (both individual and group)  (check Aesthetic Programming textbook p. 222)
- No Group/Peer feedback - MiniX[9]

---

#### 💥Class 11 | Week 17 | 25 & 27 Apr: Machine Unlearning
##### With Thur tutorial session and **NO** Fri shutup and code but with Fri Guest Speaker

**29 Apr (FRI) GUEST SPEAKER by [GEOFF COX - Machine Seeing and Invisual Literacy](https://arts.au.dk/en/aktuelt/arrangementer/vis/artikel/friday-lecture-geoff-cox-machine-seeing-and-invisual-literacy-2) @ 1400-1600**

**Preparation before the class:**

Think about this before attending the class:
> There are many things we can talk about when it comes to machine learning (or AI). Which specific aspect that interests you the most? and why? (try to substantiate your thoughts with the readings and the concrete experience that you have. - Not to just say something you feel, but substantiate and articulate it)

**Required Reading**
* Soon Winnie & Cox, Geoff, "Machine unlearning", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 227-252
* Geoff Cox, “Ways of Machine Seeing,” Unthinking Photography (2016),https://unthinking.photography/articles/ways-of-machine-seeing.

**Suggested reading**
- Kate Crawford and Vladan Joler, “Anatomy of an AI System: The Amazon Echo as an Anatomical Map of Human Labor, Data and Planetary Resources,” AI Institute (2018), https://anatomyof.ai/.
- Shakir Mohamed, Marie-Therese Png, William Isaac, “Decolonial AI: Decolonial Theory as Sociotechnical Foresight in Artificial Intelligence,” Philosophy & Technology, Springer, July 12 (2020), https://doi.org/10.1007/s13347-020-00405-8.
- Adrian Mackenzie and Anna Munster, “Platform Seeing: Image Ensembles and Their Invisualities,” Theory, Culture & Society 26, no. 5 (2019): 3-22.
- Daniel Shiffman, “Beginners Guide to Machine Learning in JavaScript,” The Coding Train, https://www.youtube.com/playlist?list=PLRqwX-V7Uu6YPSwT06y_AEYTqIwbeam3y

**In-class structure:**
- MiniX[10] walk-through - on Machine unlearning

---

#### 💥Class 12 | Week 18 | 2 & 4 May: Studio time
##### With Thur tutorial session and Fri shutup and code @ Adorno-140

NO REQUIRED READINGS

**NO PHYSICAL CLASS on 2 May (Monday) - WORK ON YOUR FINAL PROJECT**
with miniX [11] - draft of your final project

Wed:
**Group Preparation before the class:**
- 1. Prepare a 5 mins pitch of your final project
  - What's the theme you want to address?
  - What would be the readings that you will use?
  - What is the software about?
  - If you have - walkthrough your ideas via a flowchart
  - What are the challenges now?

**In-class structure:**
1. Group presentation on miniX[10]
2. 5 mins pitch about final project
3. Final project draft & studio time

---

#### 💥Class 13 | Week 19 | 9 & 11 May: SUPERVISION
##### NO Thur tutorial session and **No** Fri shutup and code

NO PHYSICAL CLASS on Monday 9 May but there are tasks to work on:

1. Take a look at all the other group final project draft
2. Prepare a detailed peer feedback for the other group within the same supervision slot for Wed class
3. Continue to work on your final project

11 May (Wed) - supervision:

| Time        | Groups                                
| ------------ |:-------------------------------------------      
| Wed (10.00-10.50) | Group 5 - Group 8
| Wed (11.00-11.50) | Group 3 - Group 4
| Wed (12.00-12.50) | Group 1 - Group 6
| Wed (13.00-13.50) | Group 7 - Group 2                     

---

#### 💥Class 14 | Week 20 | 16 May: FINAL

- ALL day final presentation
- EXAM PREP & Final evaluation

| Time        | Group                             
| ------------|:------------------------------------------  
| 09.30-09.35 | starting - intro   
| 09.40-09.53 | Group 1            
| 09.55-10.08 | Group 6                                 
| 10.10-10.23 | Group 3  
| 10.25-10.38 | Group 4  
|BREAK                          
| 10.55-11.08 | Group 5                          
| 11.10-11.23 | Group 8     
| 11.25-11.38 | Group 7     
| 11.40-11.53 | Group 2                             
|BREAK
| 12.15-13.15 | ALL - exam logistic + final evaluation

**In-class structure:**
- 8 mins presentation with demo and 5 mins Q & A for each group.
- **NOTE!! Final project submission date + Group project presentation **

## CORE TEXTBOOK:
- Soon, Winnie & Cox, Geoff. *[Aesthetic Programming: A Handbook of Software Studies](http://www.openhumanitiespress.org/books/titles/aesthetic-programming/)*. London: Open Humanities Press, 2020. (Free download)

## SKETCH INSPIRATION:
- [Daily sketch in Processing](https://twitter.com/sasj_nl) by Saskia Freeke, her talk is [here: Making Everyday - GROW 2018](https://www.youtube.com/watch?v=nBtGpEZ0-EQ&fbclid=IwAR119xLXt4nNiqpimIMWBlFHz9gJNdJyUgNwreRhIbdJMPPVx6tq7krd0ww)
- [zach lieberman](https://twitter.com/zachlieberman)
- [Creative Coding with Processing and P5.JS](https://www.facebook.com/groups/creativecodingp5/)
- [OpenProcessing](https://www.openprocessing.org/) (search with keywords)

## OTHER REFERENCES:
- [JavaScript basics](https://github.com/processing/p5.js/wiki/JavaScript-basics) by p5.js
- [NEM Programming](https://www.nemprogrammering.dk/) - provide by the student
- [Text and source code: Coding Projects with p5.js by Catherine Leung](https://cathyatseneca.gitbooks.io/coding-projects-with-p5-js/)
- [Video: Crockford on Javascript (more historical context)](https://www.youtube.com/watch?v=JxAXlJEmNMg&list=PLK2r_jmNshM9o-62zTR2toxyRlzrBsSL2)
- [A simple introduction to HTML and CSS](https://sites.google.com/sas.edu.sg/code/learn-to-code)
- McCarthy, L, Reas, C & Fry, B. *Getting Started with p5.js: Making interactive graphics in Javascript and Processing (Make)*, Maker Media, Inc, 2015.
- Shiffman, D. *Learning Processing: A Beginner’s Guide to Programming Images, Animations, and Interaction*, Morgan Kaufmann 2015 (Second Edition)
- Haverbeke,M. *[Eloquent JavaScript](https://eloquentjavascript.net/)*, 2018.
- [Video playlist: Foundations of Programming in Javascript - p5.js Tutorial by Daniel Shiffman](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA)
- [Video playlist: Intro to Programming in p5.js by Xin Xin](https://www.youtube.com/watch?v=wiG7wLwyW0E&list=PLT233rQkMw761t_nQ_6GkejNT1g3Ew4PU)

## Re-examination

Prerequisites: Submit all the required mini exercises (except miniX 9) and a final project but these have to be all done individually. (see the ref list: https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
