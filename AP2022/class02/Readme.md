# Class 02: 14 & 16 Feb | Variable Geometry

**Messy notes:**

[[_TOC_]]

## 1. MiniX - as a knowledge base
![](https://media.giphy.com/media/xTk9ZRHPeMzAO1njBC/giphy.gif)

- 💪**The concept of ReadMe and RunMe**
  - writing code & writing about code  (Aesthetic Programming & Software Studies)
  - Breaking the divide: Thinking/Doing; Theory/Practice
  - Beyond technical function of code > aesthetic and critical action + reflection
  - Low floors & High ceilings (Seymour Papert, 1980)
  - **process** (discovery journey) vs end result
  - Tips: work/discuss with your "underground" buddy
- ✍️ **Peer-Feedback**
  - Work with others [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
- 🎙Class presentation
    - not only write code but discuss and present code and software ideas
    - community: peer teaching and questioning
- Sharing/advices from last year students:
https://pad.vvvvvvaria.org/AP2021

## 2. Geometry and coordinate
![](https://pbs.twimg.com/media/FAjmlJRWUAESfGI?format=png&name=small)

Geometric Shapes / 210930, by [Saskia Freeke](https://twitter.com/sasj_nl)

- explore shapes, sizes, positions, lengths and **SPACE**
- many applications: graphic design, architecture, typography, and EMOJIs 😃

Coordinate system:

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/1-GettingStarted/ch1_11.png" width="550">

- a way to determine the position of the points > involves identification and ordering (based on numbers and mathemathics)
- 👀 frame of reference > origin > perspective > ways of seeing

`createCanvas(windowWidth,windowHeight)`

⏺️ What this `createCanvas` means? Check out the [ref guide](https://p5js.org/reference/#/p5/createCanvas)

### 🤹🏻⏺️ 2.1 **Exercise (code together):**

```javascript
function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(15);
}

function draw() {
  background(random(230,240));
}
```

### ⏺️ 2.2 Discuss the one next to you: Explain in your terms (reading ref practice):

- 🧐 what is a `frameRate`?
- 🧐 what is `random`? (check: https://p5js.org/reference/#/p5/random)

### 2.3 Example: Emoticons -> emojis 😃

- abstraction - expressive - fun
- fun -> a mode of thinking (Goriunova 2014)
- fun > not fun > what is fun?
  - blatant gender stereotyping: in professional roles
  - racial discrimination: skin tones
  - emoji standardization - universalism
  - oversimplify and universalize difference
  - representation politics -> perpetuating normative ideologies
  - inequality: how people are represented?
- see [New ‘gender equality’ emoji to show women at work](http://web.archive.org/web/20171117160218/https://www.telegraph.co.uk/technology/2016/07/15/new-gender-equality-emoji-to-show-women-at-work/)

### 2.4 Example: [AImoji](https://process.studio/works/aimoji-ai-generated-emoji/): AI-generated Emoji by Process (Design) Studio in Vienna

![](https://process.studio/wp-content/uploads/2018/12/ai-emoji_process_s_gif2.gif)
![](https://process.studio/wp-content/uploads/2018/12/process_emoji_happy-sad-1000x352.jpg)

"Using a Deep Convolutional Generative Adversarial Network (DCGAN) and a dataset consisting of 3145 individual, commonly-used Emoji as input we trained our model for 25 epochs to come up with new ones.

The resulting faces and their expressions range from expected happy/sad/angry and weird-looking to flabbergastingly horrifying ones.

We tried to categorize some of the results. Most generated Emoji are a mixture of weird/new-looking expressions, but here are examples labeled with Neutral, Horror, Happy and Sad.

To be fair, it’s pretty difficult to draw the line since there’s a visual twist to nearly all of them. After all, this ambiguity is what makes the results so appealing to us."

- The use of ML techniques and training on existing emoji data
- mess up reductive representation logic and reject universalism
- How can we think of alternatives?


## 3. Multi & Doing Programming

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/2-VariableGeometry/ch2_1.gif" height="500">

Multi by David Reinfurt (US
- web design & book cover - key visual: http://www.data-browser.net/
- [apps](http://www.o-r-g.com/apps/multi)
    - with 1,728 possible arrangements, or facial compositions, built from minimal punctuation glyphs.

⏺️ Discuss with the one next to you

1. Click the RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch2_VariableGeometry/

> (🧐) Decoding: Without looking at the source code/textbook, describe what's happening and the visual, design and interaction elements in **details**

2. WRITE IT DOWN ON A PAPER
NB: the first thing to learn is to be PRECISE!

## 4. Variables (in 3 steps)

<img src="https://media.giphy.com/media/ORzfkUjDBuxYdqXLc0/source.gif" width="400">

- storing data (kitchen container)
- local vs global

```javascript
let moving_size = 50;
let static_size = 20;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(15);
}

function draw() {
  background(random(230,240));
  ellipse(255, 350, static_size, static_size);
  ellipse(mouseX, mouseY, moving_size, moving_size);
  if (mouseIsPressed) {
    static_size = floor(random(5, 20));
  }
}
```

**✍️USAGE:**

1. **Declare:**
    - Think of a name for the container you want to store the value in
    - Declare with the syntax “let” in front.
    - There are certain rules to follow in naming variables:
        - Names usually begin with a lowercase string and not with a number or symbols.
        - Names can contain a mix of uppercase and lower case strings, and numbers.
        - Names cannot contain symbols.

2. **Initialize/Assign:**
    - What is it that you want to store? A number? By **assigning** a value, you will need to use the equal sign.
    - There are **four data types** as a beginner level:
        - **number** for numbers of any kind: integer or floating-point.
        - **string** for strings. A string may have one or more characters and it has to be used with double or single quotation marks. For example: let moving_size = "sixty";
        - **boolean** for true/false. For example: let moving_size = true;
        - **color** for color values. This accepts Red, Green, Blue (RGB) or Hue, Saturation and Brightness (HSB) values. For example: let moving_size = color(255,255,0); see more: https://p5js.org/reference/#/p5/color)

3. **(Re)Use:**
    - How and when do you want to retrieve the stored data?
    - If the variable changes over time, you may wish to reuse it many times.

### 4.1 to know the mouse position

Take a look at how mouseX and mouseY are being used, and if you want to know the coordinate of these two when you press your mouse, how to do it?

- About web console: (Under Tools > Web Developer > Web Console on Firefox).
    - Type `print(width);` and then press enter.
    - Type `console.log(width, height);` and then press enter.
    - (print vs console.log)

- Where to put `console.log(mouseX)`?

## 5. other functions

`noStroke()`, `strokeWight()`, `stroke()`, `fill()`, `nofill()`, `rect()`, `vertex()`, `floor()`, `mouseIsPressed()`

## 6. Conditional structures

<img src="https://media.giphy.com/media/DLG7XAxSMlJudB7g4r/giphy.gif" width="400">

- setting a different path by specifying conditions
- conditions - rules/boundaries - ordering/sequence/priority
- "reduce decisions to logic that leverages Boolean logic" (Ko & Wortzman 2022)
- make decision - control structures

```javascript
//example in human language
if (I am hungry) {
    eat some food;
} else if (thirsty) {
    drink some water;
} else{
    take a nap;
}
```
- check boolean expression `if() {}`

```javascript
if (mouseIsPressed) {
    static_size = floor(random(5, 20));
}
```
**RELATIONAL operators**

```javascript
if (I am hungry) && (I am in a good mood) {
    print("go out");
}
```

```javascript
/*
   Relational Operators:
   >   Greater than
   <   Less than
   >=  greater than or equal to
   <=  less than or equal to
   ==  equality
   === equality (include strict data type checking)
   !=  not equal to
   !== inequality with strict type checking
   */

   /*
   Logical Operators: boolean logic:
   &&  logical AND
   ||  logical OR
   !   logical NOT
   */

   /*
   Example:
   if () {
       //something here
   } else if() {
       //something here
   } else{
       //something here
   }
   */
```

## 7. Basic arithmetic operators

<img src="https://media.giphy.com/media/seVVu09CPz2upPeU1s/giphy.gif" width="300">

- add(+): For addition and concatenation, which is applicable to both numbers and text/characters.
- subtract(-)
- multiply(*)
- divide(/)
- Special operators: increment (++), decrement (--)

Try together:

```javascript
console.log(2*3);
```

```javascript
console.log("hello" + "world");
```

## 8. Quick test

What will be the result in the console log?

```javascript
let x = 28;
if ((x > 10) && (x <= 20)) {
    console.log("one");
} else if (x == 28) {
    console.log("two");
} else if (x === 28) {
    console.log("three");
} else if (x > 5 || x <= 30) {
    console.log("four");
} else  {
    console.log("five");
}
```
Go [here](https://www.menti.com/5y2aqq6c7k)

---
---

## 8.5 ref/Quick test

Before: - Sharing/advices from last year students:
https://pad.vvvvvvaria.org/AP2021

```JavaScript
let x;
x = random(200);
console.log(x);
```
> Would 30 be a possible value of x?
> what's it doing in first line? second line and third line?

## 9. Short presentation by students on MiniX [1]
![](https://media.giphy.com/media/l0MYS1WzKiLlEeQJq/giphy.gif)

Points to note:
- Feedback: Follow the feedback guideline [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - Establish a good "commenting" practice  (// or /*xyz*/)
  - Look for p5.js ref
  - Download the code and run at your own, try to tinker it and understand the syntax as a way of learning
  - Assigned reading reflection
- Remember MiniX is a discovery process not necessarily an end result
- MiniX is not about how complicated or many lines of code, but to make sure you understand every part of your program and to reflect on digital culture.
- ReadMe: it is ok that you don't have to 'agree' entirely to the assigned readings, but then you need to raise the debates and arguments academically.

Presentation:
1. note taking and questions: https://pad.vvvvvvaria.org/AP2022
2. Need one volunteer per presentation

## 10. Walkthrough MiniX[2] - Geometric Emoji

- See [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - See the required and suggested reading
  - BRIEF
  - FEEDBACK  

## 11. Geometric Imaginary
![](https://media.giphy.com/media/83eQIMgNvkiY/giphy.gif)

Femke Snelting's text on "Other Geometries":

> "A circle is a simple geometric shape. […] Circles are mathematically defined as the set of all points in a plane that are at the same distance from a shared center; its boundary or circumference is formed by tracing the curve of a point that keeps moving at a constant radius from the middle. […] Circles are omnipresent in practices and imaginaries of collectivity. [… and yet] Their flatness provides little in the way of vocabulary for more complex relational notions that attempt to include space, matter and time, let alone interspecies mingling and other uneasy alliances. The obligation to always stay at the same distance from the center promises a situation of equality but does so by conflating it with similarity. Circles divide spaces into an interior and an exterior, a binary separation that is never easy to overcome. We urgently need other axes to move along."

[Euclid’s Geometry Elements](https://farside.ph.utexas.edu/Books/Euclid/Elements.pdf)

- tradition of science/maths and arts
> I try to appreciate the translation of 幾何：many hows, how many... as an ambiguous term for geo 土地 and metry 測量. There are many hows as to the measuring of the earth, in relation to the measuring of your body, from your very unique standing point of view, and with a set of your own unique questions, such as “how do you remedy that with your surroundings?“, that might resonate with an army of lovers from past, present and future. you know?
However, viewing a comparative study of translations from University of Oslo, I am not ready to accept the conventional Chinese translation of Euclid’s Geometry *Elements* as *"幾何原本"*: elements became essentialized as original（原） and fundamental（本）. (with an authoritative tone hinting that it is not something to build on, experiment, or play with, like "元素". postulates as "求作者不得言不可作": not as a hypothesis or a proposal that you can contest or refuse. common notations as "公論者不可疑": undoubtable truth. (Ren Yu 2022)

> how to think about geometry differently e.g escape normative configurations, and draw attention to relations and to rethink normative geometry?

## 12. ⏺️ Discussion in class (help to conceptualize your miniX [2])

1. Examine existing geometric emojis (https://gitlab.com/aesthetic-programming/book/-/blob/master/source/2-VariableGeometry/emojis.jpeg) or those available on your mobile phone, can you describe about the shape of an emoji? what constitutes a face? What essential geometric elements do you need for a particular facial expression? What has been lost in translation?

2. Reflect upon the complexity of human emotions, and their caricatures. What is your experience using emojis? What are the cultural and political implications of emojis (you might refer to the readings e.g Aesthetic Programming- Variable Geometry and "Modifying the Universal" by Roel Roscam Abbing, Peggy Pierrot and Femke Snelting)?

3. Beyond the face, take a look at more emojis (https://www.pngfind.com/mpng/ohwmTJ_all-the-emojis-available-on-facebook-russian-revolution/). Is there anything you want to add?

Experiment with p5.js. How do you translate your thoughts into lines of code? You may want to print the coordinates of the mouse press in the web console area to get a more accurate position for your shapes.

See p5.playground by Yining Shi: https://1023.io/p5-inspector
