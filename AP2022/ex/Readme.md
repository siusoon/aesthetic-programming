Here is the protocol for the weekly RunMe (a program that can be executed) and ReadMe (contextualize your conceptual thoughts on the program) mini exercises:

[[_TOC_]]

## 📒The PROTOCOL of Weekly miniX (Both RunMe and ReadMe)📒

- **Due on every SUN mid night - unless stated otherwise** (those with late submission might miss the chance to receive feedback from your peers and fail to enter the ordinary oral exam)
- Upload 1x ReadMe and 1x RunMe on your gitlab account
- ✍️See the README's markdown guide✍️: https://about.gitlab.com/handbook/engineering/technical-writing/markdown-guide/ or - [Markdown: Basics](https://daringfireball.net/projects/markdown/basics) by John Bruger
- The Readme.md ✍️(around 3500-4000 characters):
    - 🤖 1 x screenshot (static or animated gif images, or even video if you like to best capture your work visually)
    - 🤖 1 x URL that links to your executable work (your work that can run on a web browser)
    - 🤖 1 x URL that links to your repository (at the code level)
    - 🤖 Answer/approach the questions stated in the weekly brief (**Show engagement with the topic and the theoretical, scholarly writings in the curriculm of Aesthetic Programming and/or Software Studies**)
    - 🤖 Reference URLs (if any - this is more to see where you get your inspiration and ideas)
- The RunMe (the software program):
    - js libraries
    - a HTML file
    - a js sketch file (your working file/sketch)
    - make sure your program works online
- **😎Everyone need to prepare to be asked showing and articulating your project and your peer-feedback in the class every week**

## 📝 PEER feedback📝
It is a great opportunity to learn and study together by reading and discussing other people code. Please reserve time to do the feedback every week.

Feedback is about giving a constructive response to the work and being positive. Good feedback should be specific, kind, justified and relevant as its aim is to help improve on weaknesses and strengthen what is good. Beyond this, you can refer to the additional requirement of feedback for every miniX.

🤖Check here feedback arrangement [here](https://cryptpad.fr/sheet/#/2/sheet/edit/PuPZFgguYDg6XgK89u2ve29+/)

**HOW?**
![](issues.png)
1. Go to "Issues" on his/her/their gitlab corresponding repository (locate on the left panel vertical bar).
2. Click the green 'New issue' button
3. Write the issue with the title "**Feedback on miniX(?) by (YOUR FULL NAMES)**"

## 📝 Presentation of your MiniX 📝
MiniX is an important knowledge resource for individuals and the class collectively. You will be asked to present your MiniX in the class. If you have worked with the MiniX every week seriously, you already have all the materials to present. This is not meant to add extra workload on your plate, you don't need to spend a day to prepare and just show off and describe what you have done.

**Within 15 mins** you need to demo the software, run through your code, illustrate your ideas and process, as well as to articulate your ReadMe critically in light of those theories and concepts (show engagement with the topic and the theoretical, scholarly writings in the curriculm of Aesthetic Programming and/or Software Studies). If you have explored new syntax and libraries, this would be a good time to 'teach' others how you use them in your program.

Apart from presenting the 'ready' and 'bright' side of the miniX, the course also embraces bugs/failures/mistakes, please tell us one of these (even the bug you think is obvious or silly), like how did you find out the bug and how did you "fix" it/produce workaround?

Lastly, prepare a question that can open up for class discussion.

For other classmates, we will run a collaborative notepad for each presentation and peer-feedback [here](https://pad.vvvvvvaria.org/AP2022)

## BRIEF of miniX

### 📝MiniX[11]: Final Project (Group) + draft

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/10-machine-unlearning.html#minix-final-project-93685) | due 16 May**

**✍️ 1. Draft | due on 8 May (SUN)**
- The draft should be 3-4 pages (exclude images, references and notes) in the form of a **PDF** and post to the disucssion forum (Go to BrightSpace > Course Tools > Discussion > Final Project Draft Submission > Select your group > Start a new Thread)
  - PDF of your project
    - Include a reference list
    - Include any sketch that you may have
    - Give a title to your project and make a revised flowchart

**✍️ 2. Feedback | due NEXT Wed before class:**
- Prepare an oral feedback within 8 mins
  - Use of text (assigned readings) and concepts
  - Linkage to the theme of Aesthetic Programming and the curriculum of the course
  - The clarity of the project
  - What you like the most, and why?
  - What are the less successful elements? What is your suggestion?
  - Any other comments

### 📝MiniX[10]: Machine Unlearning (Group)

due SUN mid-night

**Each group is required to prepare to talk about your miniX[10] in the following week (Wed)**

**Objective:**

- To independently explore and tinker machine learning libraries and functions with the available technical specifications
- To question and understand the existing syntaxes/logics/programming structures/functions that are related to machine learning

**Tasks (RUNME and README):**

This time you don't need to create a new concept and an artefact. What you have to do is to select one of the sample code available in the ml5.js website (for example: ImageClassifier, PoseNet, StyleTransfer, CharRNN, etc). Try to understand how the sample code works by running it and changing variable values locally on your computer. If there are new parameters that you are not familiar with, try to search online and find ways to understand some of the specific machine learning operations and technicities (e.g temperature in CharRNN, Classification in ImageClassifier, RNN, etc).  The aim of this miniX is to understand, study and discuss machine learning through engaging with computational materials, formulating search queries and reading/running computer source code.

Show some screenshots of your test, or/and your experimental sketch.

ReadMe:
- Which sample code have you chosen and why?
- Have you changed anything in order to understand the program? If yes, what are they?
- Which line(s) of code is particularly interesting to your group, and why?
- How would you explore/exploit the limitation of the sample code/machine learning algorithms?
- What are the syntaxes/functions that you don't know before? What are they and what have your learnt?
- How do you see a border relation between the code that you are tinker with and the machine learning applications in the world (e.g creative AI/ voice assistances/driving cars, bot assistants, facial/object recognition, etc)?
- What do you want to know more/ What questions can you ask further?

**✍️ NO PEER FEEDBACK is required**

### 📝MiniX[9]: Flowcharts (Individual+Group)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/9-algorithmic-procedures.html#minix-flowcharts-67471) | due SUN mid-night**
- Your own repository should point to your individual flowchart and the group one.

**✍️ NO PEER FEEDBACK is required**

### 📝MiniX[8]: E-Lit (Group)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/7-vocable-code.html#minix-e-lit-17044) | due SUN mid-night**

**✍️ 2. Feedback | due NEXT NEXT Wed before class:**
(about 3500-4000 characters)

- **🔥Describe🔥**:
    - What is the program about and how does it operate?
    - Are there any new syntax that have been used but your are not familiar with? What are they?
    - What constitutes an electronic literature (e-lit) here?
    - Does it link to the readings/concepts from the curriculum explicitly?

- **🔥Reflect🔥**:
    - Do you like the design of the program, and why/why not?
    - The way how the group structures and uses JSON is the same as how you do?
    - How would you analyze the work with the perspective of code and language that opens up and extends aesthetic reflection?

### 📝MiniX[7]: Revisit the past (individual)

**✍️ 1. BRIEF | due SUN mid-night:**

**Objective:**

- To catch up and explore further with the previous learnt functions and syntaxes
- To revisit the past and design iteratively based on the feedback and reflection on the subject matter.
- To reflect upon the methodological approach of "Aesthetic Programming"

**Tasks (RUNME and README):**
Revisit/Rework one of the earlier mini exercises that you have done, and try to focus on the methodology of Aesthetic Programming and consider technology as a site of reflection. Rework your ReadMe and RunMe:

- Which MiniX have you reworked?
- What have you changed and why?
- What have you learnt in this mini X? How did you incorporate/advance/interprete the concepts in your ReadMe/RunMe (the relation to the assigned readings)?
- What is the relation between aesthetic programming and digital culture? How does your work demonstrate the perspective of aesthetic programming (look at the Preface of the Aesthetic Programming book)?

**✍️ 2. No Peer Feedback is required

### 📝MiniX[6]: Games with objects (individual)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/6-object-abstraction.html#minix-games-with-objects-86292) | due SUN mid-night**

**✍️ 2. Feedback | due NEXT Wed before class:**
(about 3500-4000 characters)

- **🔥Describe🔥**:
    - What do you see? and What are the new / interesting syntax that have been used?
    - What are the rules and how the rules are implemented? (It is not the whole program, but more specifically the rules that lead to generativity.)
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation? (e.g generativity, rule-based system, (un)predictability, control, etc)
    - Does it link some of the concepts to the cirriculum (AP/SS) explicitly?

- **🔥Reflect🔥**:
    - Do you like the design of the program, and why/why not?
    - How does the work help you to see and think about the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)?
    - How is the thought process different when compare with your own?

### 📝MiniX[5]: A Generative Program (individual)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/5-auto-generator.html#minix-a-generative-program-26756) | due SUN mid-night**

**✍️ 2. Feedback | due NEXT Wed before class:**
(about 3500-4000 characters)

- **🔥Describe🔥**:
    - What is the game about and how do you play it?
    - What are the properties and behaviors of objects?
    - What does the work express?
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation?

- **🔥Reflect🔥**:
    - Which part of the design you like the most and why?
    - How do objects interact with the world?
    - How do worldviews and ideologies built into objects' properties and behaviors?

### 📝MiniX[4]: Capture All (individual)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/4-data-capture.html#minix-capture-all-15243) | due SUN mid-night**

**✍️ 2. Feedback | due NEXT Wed before class:**

(about 5000 characters)

- **🔥Describe🔥**:
    - What do you see? What is the program about?
    - What have been captured and what data have ben captured?
    - Does it link to any of the assigned readings explicitly? If yes, how's the conceptual linkage/reflection about the work beyond just technical description and implementation?

- **🔥Reflect🔥**:
    - Is it successful? Which aspect do you like the most? How's the thinking/approach different from your own miniX?
    - Do you have any suggestion for this miniX? (both ReadMe and RunMe)
    - How does the work change or inform your understanding of data capture?

### 📝MiniX[3]: Designing a throbber (individual)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/3-infinite-loops.html#minix-designing-a-throbber-27866) | due NEXT SUN mid-night**

**✍️ 2. Feedback | due NEXT NEXT Wed before class:**

(about 5000 characters)

- **🔥Describe🔥**:
    - What do you see? What is the program about?
    - How is it made? What (new/interesting) syntax were used?
    - What does it make you think about or feel?
- **🔥Reflect🔥**:
    - Is it successful? does it explore the prompt in a compelling, interesting or unique way?
    - Which aspect do you like the most? How's the thinking/approach different from your own miniX?
    - How does the work change or inform your understanding of (human and machine) time and temporality?
    - Can you relate and articulate the work with some of the concepts or quotes in the assigned readings?

### 📝MiniX[2]: Geometric emoji (individual)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/2-variable-geometry.html#minix-geometric-emoji-08270) | due SUN mid-night**

**✍️ 2. Feedback | due Wed before class:**

(about 5000 characters)

- **🔥Describe🔥**:
    - What do you see? What is the program about?
    - How is it made? What (new/interesting) syntax were used?
    - What does it make you think about or feel?
- **🔥Reflect🔥**:
    - Is it successful? does it explore the prompt in a compelling, interesting or unique way?
    - Which aspect do you like the most? How's the thinking/approach different from your own miniX?
    - Has the work addressed the aesthetics/politics of representation conceptually or visually? How?

### 📝MiniX[1]: RunMe and ReadMe (individual)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/1-getting-started.html#minix-runme-and-readme-28833) | due SUN mid-night**

**✍️ 2. Feedback | due Wed before class:**

(about 5000 characters)

- **🔥Describe🔥**:
    - What do you see? What is the program about?
    - How is it made? What (new/interesting) syntax were used?
    - What does it make you think about or feel?
    - Is it successful? does it explore the prompt in a compelling, interesting or unique way?
- **🔥Reflect🔥**:
    - what are the differences between reading other people code and writing your own code. What can you learn from reading other works?
    - what's the similarities and differences between writing and coding?
    - What is programming and why you need to read and write code (a question relates to coding literacy - check Annette Vee's reading)?
