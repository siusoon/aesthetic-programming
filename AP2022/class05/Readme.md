# Class 04:  7 & 9 Mar | Data Capture

**Messy notes:**

[[_TOC_]]

## 1. Various mode of capture
- data processing/capturing vs interactivity (why focus on capture?)

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/4-DataCapture/ch4_10.gif)

### 1.1 The concept of datafication:

> This term — a contraction of data and commodification — refers to the ways in which all aspects of our life seem to be turned into data which is subsequently transferred into information which is then monetized (as described by Kenneth Cukier and Victor Mayer-Schöenberger in their article “The Rise of Big Data”) - Soon & Cox 2020

> Our data, “the patterns of human behaviors,” is extracted and circulated within the logic of what Shoshana Zuboff calls “surveillance capitalism,” demonstrating the need for large quantities of all manner of data to be harvested for computational purposes, such as predictive analytic - Soon & Cox 2020

> “Data is the material produced by abstracting the world into categories, measures and other representational forms […] that constitute the building blocks from which information and knowledge are created” - (Mejias & Couldry 2021: 1)

> “data do not naturally exist, but only emerge through a process of abstraction: something is taken from things and processes, something which was not already there in discrete from before”  - (Mejias & Couldry 2021: 2)

> "“to datafy a phenomenon is to put it in quantified form so that it can be tabulated and analyzed” - (Mejias & Couldry 2021: 2)

### 1.2 ⏺️ Discussion: What is data?  

> Drawing upon the assigned readings, what is data and datafication? Why do we need to pay attention to data?

## 2. Working with the sampe code

NB: Pls use Chrome browser because the audio library is not supported by the Firefox latest version
- sample code + Exercise in class (Decode)
    - RunMe (on Chrome): https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch4_DataCapture/
    - Repository structure: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch4_DataCapture
    - speculation together (2-3 people)
        - 🧐 identify the elements, interactions and computational logics
          - list them
          - discuss

## 3. DOM element

Capture in this chapter: 1) Button/Mouse + 2) Keyboard + 3) Audio input + 4) Video input

![](https://miro.medium.com/max/700/1*z73s6aahAyEfu7bbqQ2-Fw.png)

DOM: Document Object Model -> a document like HTML with a tree structure (img src: https://javascript.plainenglish.io/the-dom-of-javascript-848506ebf386)
- e.g form elements , document style attributes
- allow programs/scripts (e.g p5.js) to dynamically access and update the content, structure and style
- What you can do in p5.js with DOM?
  - change HTML elements/attributes in the page
  - change CSS style in the page
  - react to the events in the page
- check out p5.js > DOM [here](https://p5js.org/reference/#group-DOM)

### 3.1 Binary button, CSS, Mouse capture

- RunMe (on Chrome): https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch4_DataCapture/
  - Focus on the sample code/sketch > Button
  - 🧐 Discuss the affordances of a button (ref p. 99 of the AP book)
  - 🧐 What might be the stylistic customization of the button?
- The steps to style a button and create an interactive function  
  - declare a variable with 'let'
  - now the **return type** of the varible would be p5.element objects  `createButton()`
  - You can use the available object methods e.g style, size, position, etc to change the styling and behaviors of a button

DEMO:

```javascript
let button;
function setup() {
  createCanvas(640,480);  
  button = createButton('like');
  button.style("xxx","xxx"); //e.g button.style("color", "#fff");
  button.size(100,10);
  button.position(50,50);
  //button.mousePressed(function_name1);
  //button.mouseOut(function_name2);
}
```

```javascript
//mouse capture
button.mouseOut(revertStyle);
button.mousePressed(change);

function change() {
  button.style("background", "#2d3f74");
}
function revertStyle(){
  button.style("background", "#4c69ba");
}
```

### 3.2 Keyboard capture

```javascript
function keyPressed() {
  //spacebar - check here: http://keycode.info/
  if (keyCode === 32) {
    button.style("transform", "rotate(180deg)");
  } else {   //for other keycode
    button.style("transform", "rotate(0deg)");
  }
}
```

For other related keyboard events, go to p5.js ref > Events > Keyboard [here](https://p5js.org/reference/)

The concept of listening events:
- event-driven programming
- listen for events on any element in the DOM
  - an event listener "listens" for an action
    - calls a function that performs a related tasks

### 3.3 Audio capture

This requires web audio p5.sound library
    - ❗add the sound library in the library folder
    - ❗update the index.html

```html
<script src="../libraries/p5.sound.js"></script>
```

```javascript
let mic;

function setup() {
  // Audio capture
  mic = new p5.AudioIn();
  mic.start();
}

function draw() {
  //getting the audio data i.e the overall volume (between 0 and 1.0)
  let vol = mic.getLevel();
  /*map the mic vol to the size of button,
  check map function: https://p5js.org/reference/#/p5/map */
  button.size(floor(map(vol, 0, 1, 40, 450)));
}

function change() {
  userStartAudio();
}
```

### 3.4 Video/Face capture

![](https://aesthetic-programming.net/pages/ch4_3.png)

- clmtrackr JS library (2014) by data scientist Adum M. Øygard
    - facial model -> collection of data -> machine learning training
    - face analysis -> classification -> the issues on data collection and biases
- [download a library](https://github.com/auduno/clmtrackr)
  - We need two files basically:
    1. https://github.com/auduno/clmtrackr/blob/dev/build/clmtrackr.js
    2. https://github.com/auduno/clmtrackr/blob/dev/models/model_pca_20_svm.js
    - how do we approach a new library?
    - how to locate a js library for use?
- the interaction between video capture and ctracker analysis

step 1: add libraries

```html
//add libraries
<script src="../libraries/clmtrackr/clmtrackr.js"></script>
<script src="../libraries/clmtrackr/model_pca_20_svm.js"></script>
```

step 2: create video DOM element (capture)

```javascript
let ctracker;
let capture;

function setup() {
  //web cam capture
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();
}

function draw() {
  //draw the captured video on a screen with the image filter
  image(capture, 0,0, 640, 480);
  filter(INVERT);
}
```

Step 3: using the clmtrackr library (facial recognition)

```javascript
function setup() {
  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);
}

function draw() {
  let positions = ctracker.getCurrentPosition();
  //check the availability of web cam tracking
  if (positions.length) {
     //point 60 is the mouth area
    button.position(positions[60][0]-20, positions[60][1]);

    /*loop through all major points of a face
    (see: https://www.auduno.com/clmtrackr/docs/reference.html)*/
    for (let i = 0; i < positions.length; i++) {
       noStroke();
       //color with alpha value
       fill(255,0,0,180);
       //draw ellipse at each position point
       ellipse(positions[i][0], positions[i][1], 5, 5);
    }
  }
}
```

ALL TOGETHER

```javascript
let button;
let mic;
let capture;
let ctracker;

function setup() {
  createCanvas(640,480);
  background(100);
  //button
  button = createButton('dislike');
  button.style("color", "#fff");
  button.style("background", "#4c69ba");
//  button.size(200,100);
  button.position(width/2, height/2);
  button.mousePressed(change);
  button.mouseOut(revertStyle);

  // audio
  mic = new p5.AudioIn();
  mic.start();

  //video
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.hide();

  //face ctracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

}

function draw() {
  let vol = mic.getLevel();
  //console.log(vol);
  button.size(floor(map(vol, 0, 1, 40, 500)));

  image(capture, 0,0, 640, 480);
  filter(INVERT);

  let positions  = ctracker.getCurrentPosition();

  if (positions.length) {
    button.position(positions[60][0]-20, positions[60][1]);

    for(let i=0; i< positions.length; i++) {
      noStroke();
      fill(255,0,0,180);
      ellipse(positions[i][0], positions[i][1], 5,5);
    }

  }

}

function change() {
  button.style("background", "#2d3f74");
  userStartAudio();
}

function revertStyle() {
  button.style("background", "#4c69ba");
}

//keycode..info
function keyPressed() {
  if (keyCode === 32 ) {
    button.style("transform", "rotate(180deg)");
  } else {
    button.style("transform", "rotate(0deg)");
  }
}
```

## 4. Walkthrough MiniX[4] - Capture ALL
- See [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - BRIEF
  - FEEDBACK  

**remind for the upload with new libraries such as sound and clmtrackr**

---
---

## 5 announcement

- no Shutup and code
- pls join the two Thur instructor class

## 6. ⏺️Short presentation by students on MiniX [3]

![](https://media.giphy.com/media/l0MYS1WzKiLlEeQJq/giphy.gif)

1. note taking and questions: https://pad.vvvvvvaria.org/AP2022
2. Need one volunteer per presentation

## 7. Form elements

![](https://www.queness.com/resources/images/formplugin/1.gif)

create form DOM elements in p5.js, see: https://p5js.org/reference/#group-DOM

## 8. The 2d arrays

variables -> arrays -> 2d arrays (an array of arrays)

```JavaScript
let positions  = ctracker.getCurrentPosition();

if (positions.length) {
  button.position(positions[60][0]-20, positions[60][1]);

  for(let i=0; i< positions.length; i++) {
    noStroke();
    fill(255,0,0,180);
    ellipse(positions[i][0], positions[i][1], 5,5);
  }
}
```

positions[x][y]
- 1: indicate the tracker points from 0 to 70  i.e positions[i]
- 2: retrieves the x and y coordinates of the tracker points i.e positions[i][0] or positions[i][1]

| index  | 0              | 1             |
|--------|----------------|---------------|
| 0      | x<sub>0</sub>  | y<sub>0</sub> |
| 1      | x<sub>1</sub>  | y<sub>1</sub> |
| 2      | x<sub>2</sub>  | y<sub>2</sub> |
| 3      | x<sub>3</sub>  | y<sub>3</sub> |
| 4      | x<sub>4</sub>  | y<sub>4</sub> |
| 5      | x<sub>5</sub>  | y<sub>5</sub> |
| 6      | x<sub>6</sub>  | y<sub>6</sub> |
| 7      | x<sub>7</sub>  | y<sub>7</sub> |
| 8      | x<sub>8</sub>  | y<sub>8</sub> |
| 9      | x<sub>9</sub>  | y<sub>9</sub> |
| 10     | x<sub>10</sub>  | y<sub>10</sub> |
| 11     | x<sub>11</sub>  | y<sub>11</sub> |
| 12     | x<sub>12</sub>  | y<sub>12</sub> |

... continues with 71 points in total

Example: `console.log(positions);`

<img src="2darrays.png" width=500>

## 9. ⏺️🧐 Exercise in class (modes of capture)

- try to make the sample code work in your local computer
- make sure the libary and the model are included
  1. the actual files in the libraries folder
  2. import them in the index.html

To familiar yourself with the various modes of capture, try the following:

1. Explore the various capture modes by tinkering with various parameters such as keyCode, as well as other keyboard, and mouse events.
2. Study the tracker points and try to change the position of the like button.
3. Try to test the boundaries of facial recognition (using lighting, facial expressions, and the facial composition). To what extend can a face be recognized as such, and to what extent is this impossible?
4. Do you know how the face is being modeled? How has facial recognition technology been applied in society at large, and what are some of the issues that arise from this?

## 10. ⏺️The concept of capture

🧐 discussion based on the AP book [here](https://aesthetic-programming.net/pages/4-data-capture.html#the-concept-of-capture-05196)

Select 2 from below:

- web analytics and heat map  (what are these?)
- form elements  (what's the affordances of form elements?)
- metrics of likes (what's the meaning of metrification and the values of metrics?)
- voice and audio data  (What's personalized experience? what's query data? )
- health tracker / "quantified self" (what's self-tracking and quantified self?)
Add after 2022 Spring: face tracking/surveillance and dface.app (https://dface.app/)
